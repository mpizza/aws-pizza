<?php
/*
Plugin Name: Picasa Express x2
Plugin URI: http://wott.info/picasa-express
Description: Browse, search and select photos from any publicly available Google+/Picasa Web Album and add them to your post/pages.
Version: 2.0.2
Author: gjanes
Author URI: http://www.janesfamily.org
Text Domain: pe2
Domain Path: /

Thank you to Wott (wotttt@gmail.com | http://wott.info/picasa-express) for plugin 
Picasa Express 2.0.  This plugin contained a large re-write and many improvements 
of the plugin: Picasa Image Express 2.0 RC2

Thank you to Scrawl ( scrawl@psytoy.net ) for plugin Picasa Image Express 2.0 RC2
for main idea and Picasa icons

Copyright 2013 gjanes ( email : gcj.wordpress@janesfamily.org )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

define( 'PE2_VERSION', '2.0.2' );

if (!class_exists("PicasaExpressX2")) {
	class PicasaExpressX2 {

		/**
		 * Define options and default values
		 * @var array
		 */
		var $options = array(
			'pe2_configured'        => false,
			'pe2_icon'              => 1,
			'pe2_roles'        		=> array('administrator'=>1),
			'pe2_level'         	=> 'blog',
			'pe2_user_name'         => 'undefined',

			'pe2_caption'           => 0,
			'pe2_video_overlay'     => 1,
			'pe2_title'             => 1,
			'pe2_link'              => 'thickbox_custom',
			'pe2_relate_images'     => '1',

			'pe2_img_align'         => 'left',
			'pe2_auto_clear'        => '1',
			'pe2_img_css'           => '',
			'pe2_img_style'         => '',

			'pe2_featured_tag'		=> 1,
			'pe2_additional_tags'	=> '',

			'pe2_gal_align'         => 'left',
			'pe2_gal_css'           => '',
			'pe2_gal_style'         => '',

			'pe2_img_sort'          => 0,
			'pe2_img_asc'           => 1,
			'pe2_dialog_crop'		=> 1,
			'pe2_max_albums_displayed'	=> '',

			'pe2_gal_order'         => 0,

			'pe2_token'				=> '',

			'pe2_footer_link'		=> 1,
			'pe2_donate_link'		=> 1,

			'pe2_save_state'		=> 1,
			'pe2_saved_state'		=> '',
			'pe2_last_album'		=> '',
			'pe2_saved_user_name'	=> '',

			'pe2_large_limit'		=> '',
			'pe2_single_image_size'		=> 'w400',
			'pe2_single_video_size'		=> 'w400',
		);

		/**
		 * plugin URL
		 * @var string
		 */
		var $plugin_URL;

		/**
		 * Google auth plugin URL
		 * @var string
		 */
		var $google_authorize_plugin_URL;

		/**
		 * plugin photos displayed
		 * @var array
		 */
		var $photos_displayed;

		function PicasaExpressX2() {
			// Hook for plugin de/activation
			$multisite = false;
			if (
				(function_exists('is_multisite') && is_multisite()) || // new version check
				(function_exists('activate_sitewide_plugin'))		   // pre 3.0 version check
			){
				$multisite = true;
				register_activation_hook( __FILE__, array (&$this, 'init_site_options' ) );
				register_deactivation_hook( __FILE__, array (&$this, 'delete_site_options' ) );
			} else {
				register_activation_hook( __FILE__, array (&$this, 'init_options' ) );
				register_deactivation_hook( __FILE__, array (&$this, 'delete_options' ) );
			}

			// Retrieve plugin options from database if plugin configured
			if (!$this->options['pe2_configured']) {
				// get plugin URL
				$this->plugin_URL = plugins_url().'/'.dirname(plugin_basename(__FILE__));

				// figure out the authorize URL and capitalize the URL properly to get by
				// the bug in google authorize
				$tmp = parse_url(plugins_url());
				$tmp2 = explode('.', $tmp['host']);
				$tmp2 = array_map('ucwords', $tmp2);
				$tmp_host = implode('.', $tmp2);
				$this->google_authorize_plugin_URL = $tmp['scheme'].'://'.$tmp_host.$tmp['path'].'/'.plugin_basename(__FILE__);
				unset($tmp, $tmp2, $tmp_host);

				// define the empty array used to keep track of photos displayed
				// (thus preventing duplicates in lightbox/thickbox/highslide nav
				$this->photos_displayed = array();

				foreach ($this->options as $key => $option) {
					$this->options[$key] = get_option($key,$option);
					if (!preg_match('/^[whs]\d+$/',$this->options['pe2_large_limit']))
						$this->options['pe2_large_limit'] = '';
				}

				if ($this->options['pe2_configured']) {
				if (is_admin()) {

					// loading localization if exist
					add_action('init', array(&$this, 'load_textdomain'));

					// Add settings to the plugins management page under
					add_filter('plugin_action_links_'.plugin_basename(__FILE__), array(&$this, 'add_settings_link'));
					// Add a page which will hold the  Options form
					add_action('admin_menu', array(&$this, 'add_settings_page'));
					add_filter('contextual_help', array(&$this, 'contextual_help'), 10 , 2);

					// Add media button to editor
					add_action('media_buttons', array(&$this, 'add_media_button'), 20);
					// Add iframe page creator
					add_action('media_upload_picasa', array(&$this, 'media_upload_picasa'));

					// AJAX request from media_upload_picasa iframe script ( pe2-scripts.js )
					add_action('wp_ajax_pe2_get_gallery', array(&$this, 'get_gallery'));
					add_action('wp_ajax_pe2_get_images', array(&$this, 'get_images'));
					add_action('wp_ajax_pe2_save_state', array(&$this, 'save_state'));

					// Add setting for user profile if capable
					add_action('show_user_profile', array(&$this, 'user_profile'));
					add_action('personal_options_update', array(&$this, 'user_update'));
					
					// new site creation
					if ($multisite && function_exists('get_site_option') && get_site_option('pe2_multisite')) 
						add_action('wpmu_new_blog', array(&$this, 'wpmu_new_blog') );

				} else {

					/* Attach stylesheet in the user page
					 * you can enable attach styles if define some special
					 */
					// add_action('wp_head', array(&$this, 'add_style'));

					add_shortcode('pe2-gallery', array(&$this, 'gallery_shortcode'));
					add_shortcode('pe2-image', array(&$this, 'image_shortcode'));
					add_shortcode('clear', array(&$this, 'clear_shortcode'));

					if ($this->options['pe2_footer_link']) {
						add_action('wp_footer', array(&$this, 'add_footer_link'));
					}
				}}
			}

		}

		/**
		 * Walk all blogs and apply $func to every founded
		 *
		 * @global integer $blog_id
		 * @param function $func Function to apply changes to blog
		 */
		function walk_blogs($func) {

			$walk = isset($_GET['networkwide'])||isset($_GET['sitewide']); // (de)activate by command from site admin

			if (function_exists('get_site_option')) {
				$active_sitewide_plugins = (array) maybe_unserialize( get_site_option('active_sitewide_plugins') );
				$walk = $walk || isset($active_sitewide_plugins[plugin_basename(__FILE__)]);
			}

			if ( $walk && function_exists('switch_to_blog')) {

				add_site_option('pe2_multisite', true);
				
				global $blog_id, $switched_stack, $switched;
				$saved_blog_id = $blog_id;

				global $wpdb;
				$blogs = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs WHERE public = '1' AND archived = '0' AND mature = '0' AND spam = '0' AND deleted = '0'");
				if( is_array( $blogs ) ) {
					reset( $blogs );
					foreach ( (array) $blogs as $new_blog_id ) {
						switch_to_blog($new_blog_id);
						$this->$func();
						array_pop( $switched_stack ); // clean
					}
					switch_to_blog($saved_blog_id);
					array_pop( $switched_stack ); // clean
					$switched = ( is_array( $switched_stack ) && count( $switched_stack ) > 0 );
				}
			} else {
				$this->$func();
			}
		}

		function init_site_options() {
			$this->walk_blogs('init_options');
		}
		function delete_site_options() {
			$this->walk_blogs('delete_options');
			if (function_exists('delete_site_option')) delete_site_option('pe2_multisite');
		}
		function wpmu_new_blog($new_blog_id) {
			switch_to_blog($new_blog_id);
			$this->init_options();
			restore_current_blog();
		}

		/**
		 * Enable plugin configuration and set roles by config
		 */
		function init_options() {
			add_option('pe2_configured',true);

			foreach (get_option('pe2_roles',$this->options['pe2_roles']) as $role=>$data) {
				if ($data) {
					$role = get_role($role);
					$role->add_cap('picasa_dialog');
				}
			}
		}

		/**
		 * Delete plugin configuration flag
		 */
		function delete_options() {
			delete_option('pe2_configured');
		}

		function load_textdomain() {
			if ( function_exists('load_plugin_textdomain') ) {
				load_plugin_textdomain('pe2', false, dirname( plugin_basename( __FILE__ ) ) );
			}
		}

		function add_footer_link() {
			echo "<p class=\"footer-link\" style=\"font-size:75%;text-align:center;\"><a href=\"http://wott.info/\">".__('With Picasa plugin by Geoff Janes &amp; Wott','pe2')."</a></p>";
		}

		/**
		 * Echo the link with icon to run plugin dialog
		 *
		 * @param string $id optinal id for link to plugin dialog
		 * @return void
		 */
		function add_media_button($id = '') {

			if (!current_user_can('picasa_dialog')) return;

			$plugin_URL = $this->plugin_URL;
			$icon = $this->options['pe2_icon'];
			// 'type=picasa' => 'media_upload_picasa' action above
			$media_picasa_iframe_src = "media-upload.php?type=picasa&tab=type&TB_iframe=true&width=640&height=566";
			$media_picasa_title = __("Add Picasa image or gallery", 'pe2');
			$put_id = ($id)?"id=\"$id-picasa_dialog\"":'';

			echo "<a href=\"$media_picasa_iframe_src\" $put_id class=\"thickbox\" title=\"$media_picasa_title\"><img src=\"$plugin_URL/icon_picasa$icon.gif\" alt=\"$media_picasa_title\" /></a>";

		}

		/**
		 * Config scrips and styles and print iframe content for dialog
		 *
		 */
		function media_upload_picasa() {

			if (!current_user_can('picasa_dialog')) return;

			// add script and style for dialog
			add_action('admin_print_styles', array(&$this, 'add_style'));
			add_action('admin_print_scripts', array(&$this, 'add_script'));

			// we do not need default script for media_upload
			$to_remove = explode(',', 'swfupload-all,swfupload-handlers,image-edit,set-post-thumbnail,imgareaselect');
			foreach ($to_remove as $handle) {
				if (function_exists('wp_dequeue_script')) wp_dequeue_script($handle);
				else wp_deregister_script($handle);
			}
			
			// but still reuse code for make media_upload iframe
			return wp_iframe(array(&$this, 'type_dialog'));
		}

		/**
		 * Attach script and localisation text in dialog
		 * run from action 'admin_print_scripts' from {@link media_upload_picasa()}
		 *
		 * @global object $wp_scripts
		 */
		function add_script() {
			global $wp_scripts;
			$wp_scripts->add('pe2-script', $this->plugin_URL.'/pe2-scripts.js', array('jquery'),PE2_VERSION);
			$options = array(
				'waiting'   => str_replace('%pluginpath%', $this->plugin_URL, __("<img src='%pluginpath%/loading.gif' height='16' width='16' /> Please wait", 'pe2')),
				'env_error' => __("Error: Can not insert image(s) due wrong envirionment\nCheck script media-upload.js in the parent/editor window", 'pe2'),
				'image'     => __('Image', 'pe2'),
				'gallery'   => __('Gallery', 'pe2'),
				'reload'    => __('Reload', 'pe2'),
				'options'   => __('Options', 'pe2'),
				'album'	    => __('Album', 'pe2'),
				'shortcode'	=> __('Shortcode', 'pe2'),
				'thumb_w'   => get_option('thumbnail_size_w'),
				'thumb_h'   => get_option('thumbnail_size_h'),
				'thumb_crop'=> get_option('thumbnail_crop'),
				'state'		=> 'albums',
			);
			foreach ( $this->options as $key => $val ) {
                if (!is_array($val)) // skip arrays: pe2_roles
                    $options[$key]=$val;
			}
			if ($this->options['pe2_level'] == 'user') {
				global $current_user;
				$options['pe2_save_state']  = get_user_meta($current_user->data->ID,'pe2_save_state',true);
				$options['pe2_saved_state'] = get_user_meta($current_user->data->ID,'pe2_saved_state',true);
				$options['pe2_last_album']  = get_user_meta($current_user->data->ID,'pe2_last_album',true);
				$options['pe2_saved_user_name']  = get_user_meta($current_user->data->ID,'pe2_saved_user_name',true);
				$options['pe2_user_name']   = get_user_meta($current_user->data->ID,'pe2_user_name',true);
			}

			if ($options['pe2_save_state']) {
				if ($options['pe2_saved_state']) $options['state'] = $options['pe2_saved_state'];
				if ($options['pe2_saved_user_name']) $options['pe2_user_name'] = $options['pe2_saved_user_name'];
			}
			
			$options['pe2_user_name'] = trim($options['pe2_user_name']);
			if (''==$options['pe2_user_name']) $options['pe2_user_name']='undefined';
			if ('undefined'==$options['pe2_user_name']) $options['state']= 'nouser';

			foreach ( $options as $key => $val ) {
					$options[$key] = rawurlencode($val);
			}
			$wp_scripts->localize( 'pe2-script', 'pe2_options', $options );

			$wp_scripts->enqueue('pe2-script');
		}

		/**
		 * Request styles
		 * run by action 'admin_print_styles' from {@link media_upload_picasa()}
		 *
		 * @global boolean $is_IE
		 */
		function add_style() {
			global $is_IE;
			wp_enqueue_style('media');
			wp_enqueue_style('pe2-style', $this->plugin_URL.'/picasa-express-2.css',array(),PE2_VERSION,'all');
			if ($is_IE)
				wp_enqueue_style('pe2-style-ie', $this->plugin_URL.'/picasa-express-2-IE.css',array(),PE2_VERSION,'all');
		}

		/**
		 * Print dialog html
		 * run by parameter in (@link wp_iframe()}
		 *
		 * @global object $current_user
		 */
		function type_dialog() {

			/*
				<a href="#" class="button alignright">Search</a>
				<form><input type="text" class="alignright" value="Search ..."/></form>
			 */
			?>
			<div id="pe2-nouser" class="pe2-header" style="display:none;">
				<input type="text" class="alignleft" value="user name"/>
				<a id="pe2-change-user" href="#" class="button alignleft pe2-space"><?php _e('Change user', 'pe2')?></a>
				<a id="pe2-cu-cancel" href="#" class="button alignleft pe2-space"><?php _e('Cancel', 'pe2')?></a>
				<div id="pe2-message1" class="alignleft"></div>
				<br style="clear:both;"/>
			</div>
			<div id="pe2-albums" class="pe2-header" style="display:none;">
				<a id="pe2-user" href="#" class="button alignleft"></a>
				<div id="pe2-message2" class="alignleft"><?php _e('Select an Album', 'pe2')?></div>
				<a id="pe2-switch2" href="#" class="button alignleft"><?php _e('Album', 'pe2')?></a>
				<a href="#" class="pe2-options button alignright pe2-space" ><?php _e('Options','pe2'); ?></a>
				<a href="#" class="pe2-reload button alignright" ></a>
				<br style="clear:both;"/>
			</div>
			<div id="pe2-images" class="pe2-header" style="display:none;">
				<a id="pe2-album-name" href="#" class="button alignleft"><?php _e('Select an Album', 'pe2')?></a>
				<div id="pe2-message3" class="alignleft"><?php _e('Select images', 'pe2')?></div>

				<a id="pe2-switch" href="#" class="button alignleft"><?php _e('Image', 'pe2')?></a>
				<a id="pe2-insert" href="#" class="button alignleft pe2-space" style="display:none;"><?php _e('Insert', 'pe2')?></a>
				<a href="#" class="pe2-options button alignright pe2-space" ></a>
				<a href="#" class="pe2-reload button alignright" ></a>
				<br style="clear:both;"/>
			</div>
			<div id="pe2-options" style="display:none;">
				<h3><?php _e('Image properties', 'pe2') ?></h3>
				<table class="form-table">
					<?php
					// ---------------------------------------------------------------------
					// single image thunbnail size (override)
					$option = $this->options['pe2_single_image_size'];
					preg_match('/(\w)(\d+)/',$option,$mode);
					if(strpos($option, '-c') !== false)
						$crop = true;
					else
						$crop = false;
					if (!$mode) $mode=array('','','');
					$this->make_settings_row(
						__('Single image thumbnail size', 'pe2'),
						'<input type="hidden" name="pe2_single_image_size" id="pe2_single_image_size" value="'.$option.'" />Scale: &nbsp; &nbsp;[ <label><input type="radio" name="pe2_single_image_size_mode" class="pe2_single_image_size" value="w" '.checked($mode[1], 'w', false).' /> '.__('width','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_image_size_mode" class="pe2_single_image_size" value="h" '.checked($mode[1], 'h', false).' /> '.__('height','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_image_size_mode" class="pe2_single_image_size" value="s" '.checked($mode[1], 's', false).' /> '.__('any','pe2').'</label> '.
						__(' ]&nbsp; &nbsp; proportionally to ','pe2').
						'<input type="text" name="pe2_single_image_size_dimension" class="pe2_single_image_size" style="width:60px;" id="pe2_single_image_size_dimension" value="'.$mode[2].'" />'.
						__(' pixels.','pe2').
						'<label> &nbsp; &nbsp; &nbsp; <input type="checkbox" name="pe2_single_image_size_crop" class="pe2_single_image_size" value="-c" '.checked($crop, true, false).' /> '.__(' Crop image into a square.','pe2').'</label> '
						,
						sprintf(__('Value \'%s\' will be used to set single image thumbnail size'),"<span id=\"pe2_single_image_size_message_option\">$option</span>"),
						'',
						'id="pe2_single_image_size_message" style="display:'.(($option) ? 'block' : 'none').';"'
					);
					?>
					<style type="text/css">
						input:disabled {
							background-color: #eee;
						}
					</style>
					<script type="text/javascript">
						function pe2_compute_image_size(mode,value,type) {
							var target_input = jQuery('input[name=pe2_' + type + '_size]');
							if(target_input.length == 0){
								// this is the large image size selection
								target_input = jQuery('input[name=pe2_' + type + '_limit]');
							}
							var val = target_input.val();
							
							// check for the case where it was just enabled after having
							// been disabled and saved
							if((val == '') || (val == 'w')){
								// override with some default
								val = 'w600';
							}

							// split into our parts
							var parts = {
								mode : val.substring(0, 1),
								size : val.replace(/^[a-z]*([0-9]*).*$/,'$1'),
								crop : val.replace(/^[a-z]*[0-9]*(.*)$/,'$1')};

							// override the particular part that was just changed
							parts[mode] = value;

							// store the value back in our target
							target_input.val(parts.mode+parts.size+parts.crop);

							// update the text that displays the setting being used
							jQuery('#pe2_' + type + '_size_message_option').text(parts.mode+parts.size+parts.crop);

							// if the target inputs type is hidden, then also trigger
							// the .change event (hiddens don't automatically trigger
							// the .change event for some reason)
							if(target_input.attr('type') == 'hidden'){
								target_input.trigger('change');
							}
						}// end function pe2_compute_image_size(..)
						// if mode changes, update image size
						jQuery('input[name=pe2_single_image_size_mode]').change(function(){ if (jQuery(this).attr('checked')) pe2_compute_image_size('mode',jQuery(this).val(), 'single_image'); });
						// if size changes, update image size
						jQuery('input[name=pe2_single_image_size_dimension]').change(function(){
							pe2_compute_image_size('size',jQuery('input[name=pe2_single_image_size_dimension]').val(), 'single_image');
						});
						// if crop changes, update image size
						jQuery('input[name=pe2_single_image_size_crop]').change(function(){
							pe2_determine_crop('single_image', this);
						});
						function pe2_determine_crop(name, obj){
							// use the checked selector to determine if the checkbox is 
							// checked or not
							if(jQuery('input[name=pe2_' + name + '_size_crop]:checked').length > 0){
								// the checkbox is checked
								pe2_compute_image_size('crop',jQuery(obj).val(), name);
							}else{
								// the checkbox is not checked
								pe2_compute_image_size('crop','',name);
							}
						}
					</script>
					<?php
					// ---------------------------------------------------------------------
					// single video thunbnail size (override)
					$option = $this->options['pe2_single_video_size'];
					preg_match('/(\w)(\d+)/',$option,$mode);
					if(strpos($option, '-c') !== false)
						$crop = true;
					else
						$crop = false;
					if (!$mode) $mode=array('','','');
					$this->make_settings_row(
						__('Single video thumbnail size', 'pe2'),
						'<input type="hidden" name="pe2_single_video_size" id="pe2_single_video_size" value="'.$option.'" />Scale: &nbsp; &nbsp;[ <label><input type="radio" name="pe2_single_video_size_mode" class="pe2_single_video_size" value="w" '.checked($mode[1], 'w', false).' /> '.__('width','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_video_size_mode" class="pe2_single_video_size" value="h" '.checked($mode[1], 'h', false).' /> '.__('height','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_video_size_mode" class="pe2_single_video_size" value="s" '.checked($mode[1], 's', false).' /> '.__('any','pe2').'</label> '.
						__(' ]&nbsp; &nbsp; proportionally to ','pe2').
						'<input type="text" name="pe2_single_video_size_dimension" class="pe2_single_video_size" style="width:60px;" id="pe2_single_video_size_dimension" value="'.$mode[2].'" />'.
						__(' pixels.','pe2').
						'<label> &nbsp; &nbsp; &nbsp; <input type="checkbox" name="pe2_single_video_size_crop" class="pe2_single_video_size" value="-c" '.checked($crop, true, false).' /> '.__(' Crop image into a square.','pe2').'</label> '
						,
						sprintf(__('Value \'%s\' will be used to set single video thumbnail size'),"<span id=\"pe2_single_video_size_message_option\">$option</span>"),
						'',
						'id="pe2_single_video_size_message" style="display:'.(($option) ? 'block' : 'none').';"'
					);
					?>
					<script type="text/javascript">
						// if mode changes, update image size
						jQuery('input[name=pe2_single_video_size_mode]').change(function(){ if (jQuery(this).attr('checked')) pe2_compute_image_size('mode',jQuery(this).val(), 'single_video'); });
						// if size changes, update image size
						jQuery('input[name=pe2_single_video_size_dimension]').change(function(){
							pe2_compute_image_size('size',jQuery('input[name=pe2_single_video_size_dimension]').val(), 'single_video');
						});
						// if crop changes, update image size
						jQuery('input[name=pe2_single_video_size_crop]').change(function(){
							pe2_determine_crop('single_video', this);
						});
					</script>
					<?

					$option = $this->options['pe2_caption'];
					$this->make_settings_row(
						__('Display caption', 'pe2'),
						'<label><input type="checkbox" name="pe2_caption" value="1" '.checked($option,'1',false).' /> '.__('Show the caption under thumbnail image', 'pe2').'</label> '
					);

					$option = $this->options['pe2_title'];
					$this->make_settings_row(
						__('Add caption as title', 'pe2'),
						'<label><input type="checkbox" name="pe2_title" value="1" '.checked($option,'1',false).' /> '.__('Show the caption by mouse hover tip', 'pe2').'</label> '
					);

					$opts = array (
						'none'     => __('No link', 'pe2'),
						'direct'   => __('Direct link', 'pe2'),
						'picasa'   => __('Link to Picasa Web Album', 'pe2'),
						'lightbox' => __('Lightbox', 'pe2'),
						'thickbox' => __('Thickbox (External)', 'pe2'),
						'thickbox_integrated' => __('Thickbox (Integrated Wordpress version)', 'pe2'),
						'thickbox_custom' => __('Thickbox (Custom from this plugin)', 'pe2'),
						'highslide'=> __('Highslide', 'pe2'),
					);
					$is_gallery = array (
						'none'     => 'false',
						'direct'   => 'false',
						'picasa'   => 'false',
						'lightbox' => 'true',
						'thickbox' => 'true',
						'thickbox_integrated' => 'true',
						'thickbox_custom' => 'true',
						'highslide'=> 'true',
					);
					$is_gallery_js = 'var is_gallery = { ';
					foreach ($is_gallery as $key=>$val) {
						$is_gallery_js .= "$key:$val,";
					}
					$is_gallery_js = trim($is_gallery_js, ',').' };';
					?>
					<script type="text/javascript">
					function handle_gallery_properties(t) {
						<?php echo $is_gallery_js; ?>

						if (is_gallery[t]) {
							jQuery('#gallery_properties').show();
							jQuery('#gallery-message').show();
							jQuery('#nogallery_properties').hide();
						} else {
							jQuery('#gallery_properties').hide();
							jQuery('#gallery-message').hide();
							jQuery('#nogallery_properties').show();
						}
					}
					</script>
					<?php

					$out = '<select name="pe2_link" onchange="handle_gallery_properties(this.value);">';
					$option = $this->options['pe2_link'];
					foreach ($opts as $key => $val ) {
						$out .= "<option value=\"$key\" ".selected($option, $key, false ).">$val</option>";
					}
					$out .= '</select>';
					$this->make_settings_row(
						__('Link to larger image', 'pe2'),
						$out,
						__('To use external libraries like Thickbox, Lightbox or Highslide you need to install and integrate the library independently','pe2'),
						'',
						'id="gallery-message" style="display:'.(($is_gallery[$option]=='true') ? 'block' : 'none').';"'
					);

					$option = $this->options['pe2_relate_images'];
					$this->make_settings_row(
						__('Relate all of a post\'s images', 'pe2'),
						'<label><input type="checkbox" name="pe2_relate_images" value="1" '.checked($option,'1',false).' /> '.__('If using Thickbox, Lightbox or Highslide, relate all images in the page/post together for fluid next/prev navigation', 'pe2').'</label> '
					);

					$opts = array (
						'none'   => __('None'),
						'left'   => __('Left'),
						'center' => __('Center'),
						'right'  => __('Right'),
					);
					$option = $this->options['pe2_img_align'];
					$out = '';
					foreach ($opts as $key => $val ) {
						$out .= "<input type=\"radio\" name=\"pe2_img_align\" id=\"img-align$key\" value=\"$key\" ".checked($option, $key, false)." /> ";
						$out .= "<label for=\"img-align$key\" style=\"padding-left:22px;margin-right:13px;\" class=\"image-align-$key-label\">$val</label>";
					}
					$this->make_settings_row(
						__('Image alignment', 'pe2'),
						$out
					);

					$option = $this->options['pe2_auto_clear'];
					$this->make_settings_row(
						__('Auto clear: both', 'pe2'),
						'<label><input type="checkbox" name="pe2_auto_clear" value="1" '.checked($option,'1',false).' /> '.__('Automatically add &lt;p class="clear"&gt;&lt;/p&gt; after groups of images inserted together', 'pe2').'</label> '
					);

					$this->make_settings_row(
						__('CSS Class', 'pe2'),
						'<input type="text" name="pe2_img_css" class="regular-text" value="'.esc_attr($this->options['pe2_img_css']).'"/>',
						__("You can define default class for images from theme's style.css", 'pe2')
					);
					$this->make_settings_row(
						__('Style', 'pe2'),
						'<input type="text" name="pe2_img_style" class="regular-text" value="'.esc_attr($this->options['pe2_img_style']).'"/>',
						__('You can hardcode some css attributes', 'pe2')
					);

					$this->make_settings_row(
						__('Album thumbnail size'),
						'<label for="thumbnail_size_w">'.__('Width').'</label> '.
						'<input name="thumb_w" type="text" id="thumbnail_size_w" value="'.esc_attr( get_option('thumbnail_size_w')).'" class="small-text" />&nbsp;&nbsp;&nbsp;'.
						'<label for="thumbnail_size_h">'.__('Height').'</label> '.
						'<input name="thumb_h" type="text" id="thumbnail_size_h" value="'.esc_attr( get_option('thumbnail_size_h')).'" class="small-text" /><br />'.
						'<input name="thumb_crop" type="checkbox" id="thumbnail_crop" value="1" '.checked('1', get_option('thumbnail_crop'),false).'/> '.
						'<label for="thumbnail_crop">'.__('Crop thumbnail to exact dimensions (normally thumbnails are proportional)').'</label>'
					);
					?>
				</table>

				<h3><?php _e('Gallery properties', 'pe2') ?></h3>

			<div id="nogallery_properties" style="<?php echo ($is_gallery[$this->options['pe2_link']]=='true') ? 'display:none;' : 'display:block;'?>">
				<p><?php _e('To view and change properties you have to select Thickbox, Lightbox or Highslide support for the images above', 'pe2') ?></p>
			</div>
			<div id="gallery_properties" style="<?php echo ($is_gallery[$this->options['pe2_link']]=='false') ? 'display:none;' : 'display:block;'?>">

				<table class="form-table">
					<?php
					// display tag options
					$this->make_settings_row(
						__('Photo tag options', 'pe2'),
						'<input name="pe2_featured_tag" type="checkbox" id="pe2_featured_tag" value="1" '.checked('1', get_option('pe2_featured_tag'),false).'/> '.
						'<label for="pe2_featured_tag">'.__('Include photos from this album only if they contain the "Featured" tag').'</label><br />'.
						'<label for="pe2_additional_tags" style="vertical-align: top;">'.__('Additional tag(s) required').'</label> '.
						'<div style="display: inline-block;"><input type="text" name="pe2_additional_tags" id="pe2_additional_tags" class="regular-text" style="width: 240px;" value="'.esc_attr($this->options['pe2_additional_tags']).'"/><br/><span style="font-size: 9px;">('.__('Separate multiple tags by commas.  NOTE: currently Google requires private album access for tags to work').')</span></div>'
					);

					$option = $this->options['pe2_gal_align'];
					$out = '';
					foreach ($opts as $key => $val ) {
						$out .= "<input type=\"radio\" name=\"pe2_gal_align\" id=\"gal-align$key\" value=\"$key\" ".checked($option, $key, false)." /> ";
						$out .= "<label for=\"gal-align$key\" style=\"padding-left:22px;margin-right:13px;\" class=\"image-align-$key-label\">$val</label>";
					}
					$this->make_settings_row(
						__('Gallery alignment', 'pe2'),
						$out
					);

					$this->make_settings_row(
						__('CSS Class', 'pe2'),
						'<input type="text" name="pe2_gal_css" class="regular-text" value="'.esc_attr($this->options['pe2_gal_css']).'"/>',
						__("You can define default class for images from theme's style.css", 'pe2')
					);
					$this->make_settings_row(
						__('Style', 'pe2'),
						'<input type="text" name="pe2_gal_style" class="regular-text" value="'.esc_attr($this->options['pe2_gal_style']).'"/>',
						__('You can hardcode some css attributes', 'pe2')
					);
					?>

				</table>
			</div>

			</div>
			<div id="pe2-main">
			</div>
		<?php
		}

		/**
		 * Request server with token if defined
		 *
		 * @param string $url URL for request data
		 * @param boolean $token use token from settings
		 * @return string received data
		 */
		function get_feed($url,$token=false) {
			global $wp_version;
			// add Auth later
			$options = array(
				'timeout' => 30 ,
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
				'sslverify' => false // prevent some problems with Google in token request
			);

			if (!$token) {
				if ($this->options['pe2_level'] == 'user') {
					global $current_user;
					$token = get_user_meta($current_user->data->ID,'pe2_token',true);
				} else {
					$token  = $this->options['pe2_token'];
				}
			}
			if ($token) $options['headers'] = array ( 'Authorization' =>"AuthSub token=\"$token\"" );

			$response = wp_remote_get($url, $options);

			if ( is_wp_error( $response ) )
				return $response;

			if ( 200 != $response['response']['code'] )
				return new WP_Error('http_request_failed', __('Response code is ').$response['response']['code']);

			// preg sensitive for \n\n, but we not need any formating inside
			return (str_replace("\n",'',trim( $response['body'] )));
		}

		/**
		 * Find tag in content by attribute
		 *
		 * @param string $content
		 * @param string $tag
		 * @param string $attr
		 * @return string attribute value or all parameters if not found. false if no tag found
		 */
		function get_item_attr($content,$tag,$attr) {
			if (!preg_match("|<$tag\s+([^>]+)/?>|u",$content,$m))
				return false;
			$a = preg_split("/[\s=]/",$m[1]);
			for ($i=0; $i<count($a); $i+=2) {
				if ($a[$i]==$attr) return trim($a[$i+1],"'\" ");
			}
			return join(',',$a);
		}
		/**
		 * Find tag in content
		 *
		 * @param string $content
		 * @param string $tag
		 * @param boolean $first Search only first. False by default
		 * @return bool|string|array content of the found node. false if not found
		 */
		function get_item($content,$tag,$first=false) {
			if (!preg_match_all("|<$tag(?:\s[^>]+)?>(.+?)</$tag>|u",$content,$m,PREG_PATTERN_ORDER))
				return false;
//			echo "$tag: ".count($m[1])."<br/>";
			if (count($m[1])>1 && !$first) return ($m[1]);
			else return ($m[1][0]);
		}

		/**
		 * wp_ajax_pe2_get_gallery
		 * print html for gallery
		 *
		 */
		function get_gallery() {

			if (!current_user_can('picasa_dialog')) {
				echo json_encode((object) array('error'=>__('Insufficient privelegies','pe2')));
				die();
			}

			$out = (object)array();

			if (isset($_POST['user'])) {
				$user = $_POST['user'];
			} else die();

			$rss = $this->get_feed("http://picasaweb.google.com/data/feed/base/user/$user?alt=rss&kind=album&hl=en_US");
			if (is_wp_error($rss)) {
				$out->error = $rss->get_error_message();
			} else if (!$this->get_item($rss,'atom:id')) {
				$out->error = __('Invalid picasa username: ', 'pe2').$user;
		    } else {
			    $items = $this->get_item($rss,'item');
			    $output = '';
			    if ($items) {
			    	if (!is_array($items)) $items = array($items);
			    	$output .= "\n<table><tr>\n";
			    	$i = 0;
					$max_albums = get_option('pe2_max_albums_displayed');
					foreach($items as $item) {
						// http://picasaweb.google.com/data/entry/base/user/wotttt/albumid/5408701349107410241?alt=rss&amp;hl=en_US
						$guid  = str_replace("entry","feed",$this->get_item($item,'guid'))."&kind=photo";
						$title = $this->escape($this->get_item($item,'title'));
						$desc  = $this->escape($this->get_item($item,'media:description'));
						$url   = $this->get_item_attr($item,'media:thumbnail','url');
						$item_type  = (strpos($item, 'medium=\'video\'') !== false ? 'video' : 'image');

						// resize the thumbnail URL so that it fits properly in the media
						// window
						$url = str_replace('s160-c', 's140-c', $url);

						// generate the output
						$output .= "<td><a href='#$guid'><img src='$url' alt='$desc' type='$item_type'/><span>$title</span></a></td>\n";

						// increment the shared image counter for the following
						// two checks
						$i++;

						// determine if we need to stop outputting albums
						if(($max_albums > 0) && ($i >= $max_albums)){
							// we've reached our max, break out of the loop
							break;
						}

						// determine if we need to break this row and start a new
						// one
						if ($i % 4 == 0) $output .= "</tr><tr>\n";
					}// end foreach album item to output
					$output .= "</tr></table>\n";
			    }// end if we have items to output

			    $out->items = $this->get_item($rss,'openSearch:totalResults');
			    $out->title = $this->get_item($rss,'title',true);
			    $out->data  = $output;
			    $out->cache  = $_POST['cache'];
		    }// end else for if there were any errors

			echo json_encode($out);
			die();
		}

		/**
		 * wp_ajax_pe2_get_images
		 * print html for images
		 *
		 */
		function get_images() {

			if (!current_user_can('picasa_dialog')) {
				echo json_encode((object) array('error'=>__('Insufficient privelegies','pe2')));
				die();
			}

			$out = (object)array();

			if (isset($_POST['guid'])) {
				// determine if this guid is base64 encoded or a straight album URL,
				// decoding if necessary
				if(strpos($_POST['guid'], 'http') !== 0){
					// decode it
					$album = base64_decode($_POST['guid']);
				}else{
					// simply store it after decoding any entities that may have been
					// created by the editor or elsewhere
					$album = html_entity_decode($_POST['guid']);
				}
			} else die();

			$rss = $this->get_feed($album);
			if (is_wp_error($rss)) {
				$out->error = $rss->get_error_message();
			} else if (!$this->get_item($rss,'atom:id')) {
				$out->error = __('Invalid album ', 'pe2');
		    } else {
			    $items = $this->get_item($rss,'item');
		    	$output = '';
		    	$key = 1; $images = array();
		    	$sort = $this->options['pe2_img_sort'];
		    	$dialog_crop = ($this->options['pe2_dialog_crop'] == 1 ? '-c' : '');
			    if ($items) {
			    	if (!is_array($items)) $items = array($items);
			    	foreach($items as $item) {
			    		switch ($sort) {
			    			case 0: $key++; break;
			    			case 1: $key = strtotime($this->get_item($item,'pubDate',true)); break;
			    			case 2: $key = $this->get_item($item,'title',true); break;
			    			case 3: $key = $this->get_item($item,'media:title',true); break;
			    		}
			    		$images[$key] = array (
						'album'  => $this->get_item($item,'link'), // picasa album image
						'title' => $this->escape($this->get_item($item,'title')),
						'file'  => $this->escape($this->get_item($item,'media:title')),
						'desc'  => $this->escape($this->get_item($item,'media:description')),
						'item_type'  => (strpos($item, 'medium=\'video\'') !== false ? 'video' : 'image'),
						'url'   => str_replace('s72','s144'.$dialog_crop,$this->get_item_attr($item,'media:thumbnail','url')),
			    		);
			    	}
			    	if ($this->options['pe2_img_asc']) ksort($images);
			    	else krsort($images);
			    	$output .= "\n<table><tr>\n";
			    	$i = 0;
			    	foreach($images as $item) {
						$output .= "<td><a href='{$item['album']}'><img src='{$item['url']}' alt='{$item['file']}' type='{$item['item_type']}' title='{$item['desc']}' /><span>{$item['title']}</span></a></td>\n";
						if ($i++%4==3) $output .= "</tr><tr>\n";
					}
					$output .= "</tr></table>\n";
			    }

				// do our action for dialog footer
				$output = apply_filters('pe2_get_images_footer', $output);

				// add our successful results to the output to return
			    $out->items = $this->get_item($rss,'openSearch:totalResults');
			    $out->title = $this->get_item($rss,'title',true);
				$out->data  = $output;
			    $out->cache  = $_POST['cache'];
		    }// end else for if we had an error getting the images

			// output the result and exit
			echo json_encode($out);
			die();

		}

		/**
		 * Escape quotes to html entinty
		 *
		 * @param <type> $str
		 * @return <type>
		 */
		function escape($str) {
			$str = preg_replace('/"/', '&quot;', $str);
			$str = preg_replace("/'/", '&#039;', $str);
			return $str;
		}

		/**
		 * wp_ajax_pe2_save_state
		 * save state of dialog
		 */
		function save_state() {
			if (!current_user_can('picasa_dialog')) {
				echo json_encode((object) array('error'=>__('Insufficient privelegies','pe2')));
				die();
			}

			if (!isset($_POST['state'])) die();
			global $current_user;

			switch ( $saved_state = sanitize_text_field($_POST['state']) ) {
				case 'nouser' :
				case 'albums' :
					if ($this->options['pe2_level'] == 'user')
						update_user_meta($current_user->data->ID, 'pe2_saved_user_name', sanitize_text_field($_POST['last_request']) );
					else
						update_option( 'pe2_saved_user_name', sanitize_text_field($_POST['last_request']) );
					break;
				case 'images' :
					if ($this->options['pe2_level'] == 'user')
						update_user_meta($current_user->data->ID, 'pe2_last_album', sanitize_text_field($_POST['last_request']) );
					else
						update_option( 'pe2_last_album', sanitize_text_field($_POST['last_request']) );
					break;
				default:
					die();
			}
			if ($this->options['pe2_level'] == 'user')
				update_user_meta($current_user->data->ID, 'pe2_saved_state', $saved_state );
			else
				update_option( 'pe2_saved_state', $saved_state );
			die();
		}

		/**
		 * Envelope content with tag
		 * used by shortcode 'pe2_gallery'
		 *
		 * @param array $atts tag, class and style defined. album also
		 * @param string $content
		 * @return string
		 */
		function gallery_shortcode($atts, $content) {
			extract(shortcode_atts(array(
				'class' => $this->options['pe2_gal_css'],
				'style' => $this->options['pe2_gal_style'],
				'align' => $this->options['pe2_gal_align'],
				
				'tag'   => 'div',
				'album' => '',
				
				'thumb_w' => get_option('thumbnail_size_w'),
				'thumb_h' => get_option('thumbnail_size_h'),
				'thumb_crop' => get_option('thumbnail_crop'),
				
				'img_align'	=> $this->options['pe2_img_align'],
				'img_class'	=> $this->options['pe2_img_css'],
				'img_style'	=> $this->options['pe2_img_style'],
				'img_sort'	=> $this->options['pe2_img_sort'],
				'img_asc'	=> $this->options['pe2_img_asc'],
				
				'caption'	=> $this->options['pe2_caption'],
				'pe2_title'	=> $this->options['pe2_title'],
				
				'link'	=> $this->options['pe2_link'],
				'relate_images'	=> $this->options['pe2_relate_images'],
				'large_size' => $this->options['pe2_large_limit'],
				
				'limit' => '',
				'hide_rest'  => ''
			), $atts ));

			if ($album) {
				// request images for album - generate the request URL
				if(strpos($album, 'http') !== 0){
					// for backwards compatibility, decode the base64 encoded album data
					// stored in the tag
					$feed_url = base64_decode($album);
				}else{
					// simply store the album url after decoding any entities created by
					// the visual/HTML editor
					$feed_url = html_entity_decode($album);
				}

				// determine if we have any tags to send with the query
				if(isset($atts['tags'])){
					// we also have tags to query, append them
					$feed_url .= '&tag='.urlencode($atts['tags']).'&orderby=date';
				}

				// grab the data and process it
				$rss = $this->get_feed($feed_url);
				if (is_wp_error($rss)) {
					$content = $rss->get_error_message();
				} else if ($this->get_item($rss,'atom:id')) {
					$items = $this->get_item($rss,'item');
					$output = '';

					// determine if we're relating all images, or just those
					// in this gallery
					if($relate_images){
						// use the per-post unique ID so all images in the post
						// are related
						$uniqid = 'post-'.get_the_ID();
					}else{
						// generate a unique id for this gallery
						$uniqid = uniqid('');
					}

					// prepare common image attributes
					$iclass = explode(' ',$img_class);
					$istyle = array($img_style);

					// create align vars
					// for caption - align="alignclass" including alignnone also
					$calign = '';
					if ($caption) {
						$calign = 'align="align'.$img_align.'" ';
					}

					// new size for thumbnail
					$new_thumb_size = '';
					if ($thumb_w && $thumb_h) {
						// both sizes and crop
						if ($thumb_w == $thumb_h) {
							if ($thumb_crop) $new_thumb_size = '/s'.$thumb_w.'-c';
							else $new_thumb_size = '/s'.$thumb_w;
						}
						else if ($thumb_w > $thumb_h) $new_thumb_size = '/w'.$thumb_w;
						else $new_thumb_size = '/h'.$thumb_h;
					}
					else if ($thumb_w) $new_thumb_size = '/w'.$thumb_w;
					else if ($thumb_h) $new_thumb_size = '/h'.$thumb_h;

					// new size for large image
					$new_large_size='/s0';
					if ($large_size) $new_large_size = '/'.$large_size;

					$cdim  = ($thumb_w)?('width="'.$thumb_w.'" '):'';

					// link and gallery additions
					$amore='';
					switch ($link) {
						case 'thickbox':
						case 'thickbox_integrated':
						case 'thickbox_custom':
							$amore = 'class="thickbox" ';
							if (true) $amore .= 'rel="'.$uniqid.'" ';
							break;
						case 'lightbox':
							$amore = (true)?('rel="lightbox-'.$uniqid.'" '):'rel="lightbox" ';
							break;
						case 'highslide':
							$amore = (true)?('class="highslide" onclick="return hs.expand(this,{ slideshowGroup: \''.$uniqid.'\' })"'):
								'class="highslide" onclick="return hs.expand(this)"';
							break;
					}

					$iclass = implode(' ',array_diff($iclass,array(''))); $iclass = ($iclass)?('class="'.$iclass.'" '):'';
					$istyle = implode(' ',array_diff($istyle,array(''))); $istyle = ($istyle)?('style="'.$istyle.'" '):'';
					
					$key = 1; $images = array();
					
					if ($items) {
						if (!is_array($items)) $items = array($items);

						// if we're searching by tags, the RSS feed returned the results
						// in the order of most-recent first, which doesn't make very
						// much sense when considering how this works.  reverse teh 
						// array order
						if(isset($atts['tags'])){
							// we searched for tags and messed up the order, reverse it
							$items = array_reverse($items);
						}

						// loop through each and build the HTML
						foreach($items as $item) {
							switch ((string)$img_sort) {
								case 'None':
								case 'none':
								case '0': $key++; break;
								case 'date':
								case '1': $key = strtotime($this->get_item($item,'pubDate',true)); break;
								case 'Title':
								case 'title':
								case '2': $key = $this->get_item($item,'title',true); break;
								case 'File name':
								case 'File':
								case 'file':
								case '3': $key = $this->get_item($item,'media:title',true); break;
								case 'Random':
								case 'random':
								case '4': $key = rand(); break;
								default: $key++; break;
							}
							$url = $this->get_item_attr($item,'media:thumbnail','url');
							$title = $this->escape($this->get_item($item,'title'));
							$picasa_link = $this->get_item($item, 'link');
							$images[$key] = array (
								'ialbum'   => $this->get_item($item,'link'), // picasa album image
								'icaption' => $title,
								'ialt'     => $this->escape($this->get_item($item,'media:title')),
								'isrc'     => str_replace('/s72',$new_thumb_size,$url),
								'iorig'    => str_replace('/s72',$new_large_size,$url),
								'ititle'   => ($pe2_title)?'title="'.$title.'" ':'',
								'ilink'    => $picasa_link,
//FIXME - CSS needs to be corrected
								//'itype'	   => (strpos($item, 'medium=\'video\'') !== false ? 'video' : 'image')
								'itype'	   => ''
							);
							if ($limit && !$hide_rest) {
								if (++$count>=$limit) break;
							}
						}// end foreach items to process
						if ($img_asc) ksort($images);
						else krsort($images);
						
						if ($limit && $hide_rest && $limit==absint($limit)) $count=0;
						else $limit=false;
						
						foreach($images as $item) {
							$img = "<img src=\"{$item['isrc']}\" alt=\"{$item['ialt']}\" type=\"{$item['itype']}\" {$item['ititle']}{$iclass}{$istyle} />";

							if ($link != 'none') {
								if ($link == 'picasa') $item['iorig'] = $item['ialbum'];

								// determine if this particular link has been displayed
								// already or not (to prevent multiple copies related
								// to each other from busting the navigation)
								if(in_array($item['iorig'], $this->photos_displayed)){
									// this photo has already been displayed, skip relating
									// it to the rest and instead make up a new relationship
									// for it so that we don't break the navigation
									$amore_this = str_replace($uniqid, uniqid(), $amore);
								}else{
									// this photo hasn't been displayed yet, it can be related
									// without issue
									$amore_this = $amore;
								}

								// store this photo in our list of displayed photos
								$this->photos_displayed[] = $item['iorig'];

								// create the image link
								$img = "<a href=\"{$item['iorig']}\" link=\"{$item['ilink']}\" {$item['ititle']}{$amore_this}>$img</a>";
							}
							if ($caption) {
								// add caption
								$img = "[caption id=\"\" {$calign}{$cdim}caption=\"{$item['icaption']}\"]{$img}[/caption] ";
							}

							$output .= $img;
							
							if ($limit) {
								if (++$count>=$limit) {
									$istyle=$hstyle;
									$amore .= ' style="display:none;"';
									$caption=false;
								}
							}
						}

						$class = array_merge( array(($align!='none')?'align'.$align:''), explode(' ',$class) );
						$class = array_diff($class, array('') );
						$class = implode(' ', $class );

					}
				}
				$content .= $output;
			}

			$code = "<$tag class=\"pe2-album $pe2_gal_class\" style=\"$pe2_gal_style\">".do_shortcode($content)."</$tag><div class='clear'></div>";

			return $code;
		}

		/**
		 * Envelope content with tag
		 * used by shortcode 'pe2_image'
		 *
		 * @param array $atts tag, class and style defined.
		 * @param string $content
		 * @return string
		 */
		function image_shortcode($atts, $content) {
			// extract all of the variables from defaults/options with
			// any tag attribute overrides
			extract(shortcode_atts(array_merge(array(
						'src'	=> '',
						'href'	=> '',
						'caption'	=> '',
						'type'	=> '',
						'alt'	=> '',
						'limit' => '',
						'hide_rest'  => ''
					), $this->options
				), $atts )
			);

			// create align vars
			// for caption - align="alignclass" including alignnone also
			// else add alignclass to iclass
			$calign = '';
			$iclass = array();
			if ($pe2_caption) {
				$calign = 'align="align'.$pe2_img_align.'" ';
			} else {
				array_push($iclass,'align'.$pe2_img_align);
			}

			// generate the unique id if we're relating images
			$uniqid = 'post-'.get_the_ID();

			// link and gallery additions
			$a_link_additions = '';
			switch ($pe2_link) {
				case 'thickbox':
				case 'thickbox_integrated':
				case 'thickbox_custom':
					$a_link_additions = 'class="thickbox" ';
					if($pe2_relate_images){
						// they have chosen to relate all of the images, use the post id
						$a_link_additions .= 'rel="'.$uniqid.'" ';
					}
					break;
				case 'lightbox':
					if($pe2_relate_images){
						// they have chosen to relate all of the images, use the post id
						$a_link_additions = 'rel="lightbox-'.$uniqid.'" ';
					}else{
						// separate images without navigation
						$a_link_additions = 'rel="lightbox" ';
					}
					break;
				case 'highslide':
					if($pe2_relate_images){
						// they have chosen to relate all of the images, use the post id
						$a_link_additions = 'class="highslide" onclick="return hs.expand(this,{ slideshowGroup: \''.$uniqid.'\' })"';
					}else{
						// separate images without navigation
						$a_link_additions = 'class="highslide" onclick="return hs.expand(this)"';
					}
					break;
			}// end switch
			
			// determine the type and then set the thumbnail url
			$amore = '';
			$imore = '';
			if($type == 'image'){
				// use the image size
				$thumb_size = $pe2_single_image_size;

				// set the link href to the large size.  determine if the
				// size has been defined, or if we just use the default
				if($pe2_large_limit == null){
					// none set, use a default
					$large_size = 's0';
				}else{
					// use the large limit from the configuration
					$large_size = $pe2_large_limit;
				}

				// create the a link, linking to the larger version of the image
				$a_href = preg_replace('/\/(w|h|s)[0-9]+(-c|)\//', '/'.$large_size.'/', $src);

				// set the amore to our a_link_additions
				$amore = $a_link_additions;
			}else{
				// use the video size
				$thumb_size = $pe2_single_video_size;

				// set the link href to the picasa HREF
				$a_href = $href;

				// set the amore to make it open in a new tab and mark it as a video
				// type, and not add in the a_link_additions configuration
				$amore .= ' target="_blank" type="video"';
				$imore = ' type="video"';
			}// end else for if we're displaying an image

			// generate the URL for the thumbnail image
			$thumb_src = preg_replace('/\/(w|h|s)[0-9]+(-c|)\//', '/'.$thumb_size.'/', $src);

			// add our pe2 class to the image class
			$iclass[] = 'pe2-photo';

			// generate the other image attributes we need
			$ititle = ($pe2_title) ? 'title="'.$caption.'" ' : '';
			$iclass = implode(' ', $iclass);
			if($pe2_img_css){
				$iclass .= ' '.$pe2_img_css;
			}
			if($iclass){
				$iclass = 'class="'.$iclass.'" ';
			}
			if($pe2_img_style){
				$istyle = 'style="'.$pe2_img_style.'" ';
			}else{
				$istyle = '';
			}

			// create the HTML for the image tag
			$html = "<img src=\"{$thumb_src}\" alt=\"{$alt}\" {$ititle}{$iclass}{$istyle}{$imore} />";

			// add the link?
			if ($pe2_link != 'none') {
				// the image should also have a link, determine if this particular 
				// link has been displayed already or not (to prevent multiple 
				// copies related to each other from busting the navigation)
				if(in_array($a_href, $this->photos_displayed)){
					// this photo has already been displayed, skip relating
					// it to the rest and instead make up a new relationship
					// for it so that we don't break the navigation
					$amore_this = str_replace($uniqid, uniqid(), $amore);
				}else{
					// this photo hasn't been displayed yet, it can be related
					// without issue
					$amore_this = $amore;
				}

				// store this photo in our list of displayed photos
				$this->photos_displayed[] = $a_href;
				
				// figure out what the link is
				if($link == 'picasa'){
					// the large_url gets switched for the href
					$a_href = $href;
				}

				// wrap the current image tag with the A tag, adding the "link" 
				// attribute so the thickbox-custom can add the link to picasa
				$html = "<a href=\"{$a_href}\" link=\"{$href}\" {$ititle}{$amore_this}>$html</a>";
			}// end if we need to add the link

//FIXME - this functionality has not been tested yet
			if ($pe2_caption) {
				// add caption
				$html = "[caption id=\"\" {$calign}caption=\"{$title}\"]{$html}[/caption] ";
			}

			// return our processed shortcode with teh image link
			return do_shortcode($html);
		}// end function image_shortcode(..)

		/**
		 * Envelope content with tag with additinoal class 'clear'
		 * used by shortcode 'clear'
		 *
		 * @param array $atts tag and class
		 * @param string $content
		 * @return string
		 */
		function clear_shortcode($atts, $content) {
			extract(shortcode_atts(array(
				'class' => '',
				'tag'   => 'div',
			), $atts ));

			$class .= (($class)?' ':'').'clear';

			$code = "<$tag class='$class'>".do_shortcode($content)."</$tag>";

			return $code;
		}

		/**
		 * Print and request user for Picasa in profile. Token link present
		 * uses if settings in user level
		 * run by action 'show_user_profile' from user-edit.php
		 *
		 * @param object $user
		 */
		function user_profile($user) {

			if (!current_user_can('picasa_dialog')) return;
			if ($this->options['pe2_level'] != 'user') return;

			$user_id = $user->ID;

			if ( isset($_GET['revoke']) ) {
				$response = $this->get_feed("https://www.google.com/accounts/AuthSubRevokeToken");
				if ( is_wp_error( $response ) ) {
					$message = __('Google return error: ','pe2').$response->get_error_message();
				} else {
					$message = __('Private access revoked','pe2');
				}
				delete_user_meta($user_id,'pe2_token');
				$this->options['pe2_token'] = '';
			}

			if ( isset($_GET['message']) && $_GET['message']) {
				$message = esc_html(stripcslashes($_GET['message']));
			}


			if (!get_user_meta($user_id,'pe2_user_name',true) && current_user_can('manage_options') ) {
				update_user_meta($user_id,'pe2_user_name',$this->options['pe2_user_name']);
				if ($this->options['pe2_token'])
					update_user_meta($user_id,'pe2_token',$this->options['pe2_token']);
			} 

			?>
				<h3><?php _e('Picasa access', 'pe2') ?></h3>

				<?php
					if ($message) {
						echo '<div id="picasa-express-x2-message" class="updated"><p><strong>'.$message.'</strong></p></div>';
					}
				?>

				<table class="form-table">
					<?php
					$user = get_user_meta($user_id,'pe2_user_name',true);
					$result = 'ok';
					$response = $this->get_feed("http://picasaweb.google.com/data/feed/base/user/$user?alt=rss&kind=album&hl=en_US");
					if ( is_wp_error( $response ) )
						$result = 'nok: '.$response->get_error_message();
				    else if (!$this->get_item($response,'atom:id')) {
						$result = 'nok: wrong answer';
					}

					if (method_exists('WP_Http', '_getTransport')) {
						$ta = array(); $transports = WP_Http::_getTransport(array());
						foreach ($transports as $t) $ta[] = strtolower(str_replace('WP_Http_','',get_class($t)));
						if ($ta) $result = sprintf(__("checking user: %s transport: %s",'pe2'),$result,implode(',',$ta));
					} else if (method_exists('WP_Http', '_get_first_available_transport')) {
						$transport = WP_Http::_get_first_available_transport(array());
						if ($transport) {
							$transport_name = strtolower(str_replace('WP_HTTP_','',$transport));
							$result = sprintf(' '.__("checking user: %s - transport: %s",'pe2'),$result,$transport_name);
						}
					} else {
						$result = '';
					}

					$this->make_settings_row(
						__('Picasa user name', 'pe2'),
						'<input type="text" class="regular-text" name="pe2_user_name" value="'.esc_attr($user).'" />'.$result.
						((!get_user_meta($user_id,'pe2_token',true))?'<br /><a href="https://www.google.com/accounts/AuthSubRequest?next='.urlencode($this->google_authorize_plugin_URL.'?authorize&user='.$user_id).'&scope=http%3A%2F%2Fpicasaweb.google.com%2Fdata%2F&session=1&secure=0">'.__('Requesting access to private albums', 'pe2').'</a>':'<br/><a href="?revoke=true">'.__('Revoke access to private albums', 'pe2').'</a>'),
						((get_user_meta($user_id,'pe2_token',true))?__('You already received the access to private albums', 'pe2'):__('By this link you will be redirected to the Google authorization page. Please, use same name as above to login before accept.', 'pe2'))
					);
					$option = get_user_meta($user_id,'pe2_save_state',true);
					$this->make_settings_row(
						__('Save last state', 'pe2'),
						'<label><input type="checkbox" name="pe2_save_state" value="1" '.checked($option,'1',false).' /> '.__('Save last state in dialog', 'pe2').'</label> ',
						__('Save user when changes, album if you insert images or albums list if you shorcode for album', 'pe2')
					);
					?>
				</table>
			<?php
		}

		/**
		 * Save parameters and save profile
		 * by action 'personal_options_update' in user-edit.php
		 */
		function user_update() {

			if (!current_user_can('picasa_dialog')) return;

			$user_id = sanitize_text_field($_POST['user_id']);
			if ($user_id && isset($_POST['pe2_user_name']) && sanitize_text_field($_POST['pe2_user_name']) != get_user_meta($user_id,'pe2_user_name',true)) {
				$picasa_user = sanitize_text_field($_POST['pe2_user_name']);
				if (!$picasa_user) $picasa_user='undefined';
				update_user_meta($user_id,'pe2_user_name', $picasa_user);
				delete_user_meta($user_id,'pe2_token');
			}
			update_user_meta($user_id,'pe2_save_state', ((isset($_POST['pe2_save_state']) && $_POST['pe2_save_state'])?'1':'0'));
		}

		/**
		 * Add setting link to plugin action
		 * run by action 'plugin_action_links_*'
		 *
		 */
		function add_settings_link($links) {
			if (!current_user_can('manage_options')) return $links;
			$settings_link = '<a href="options-general.php?page=picasa-express-2">'.__('Settings', 'pe2').'</a>';
			array_unshift( $links, $settings_link );
			return $links;
		}

		/**
		 * Config settings, add actions for registry setting and add styles
		 * run by action 'admin_menu'
		 *
		 */
		function add_settings_page() {
			if (!current_user_can('manage_options')) return;
			add_options_page(__('Picasa Express x2', 'pe2'), __('Picasa Express x2', 'pe2'), 'manage_options', 'picasa-express-2', array(&$this, 'settings_form'));
			add_action('admin_init', array(&$this, 'settings_reg'));
			add_action('admin_print_styles-settings_page_picasa-express-2', array(&$this, 'settings_style'));
		}

		/**
		 * Register all option for save
		 *
		 */
		function settings_reg() {
			foreach ($this->options as $key => $option) {
				if ($key != 'pe2_token') // skip token in non secure requests
					register_setting( 'picasa-express-2', $key );
			}
		}

		/**
		 * Define misseed style for setting page
		 */
		function settings_style() {
			$images = admin_url('images');
			echo<<<STYLE
			<style type="text/css" id="pe2-media" name="pe2-media">
				.image-align-none-label {
					background: url($images/align-none.png) no-repeat center left;
				}
				.image-align-left-label {
					background: url($images/align-left.png) no-repeat center left;
				}
				.image-align-center-label {
					background: url($images/align-center.png) no-repeat center left;
				}
				.image-align-right-label {
					background: url($images/align-right.png) no-repeat center left;
				}
			</style>
STYLE;
		}

		/**
		 * Add help to the top of the setting page
		 */
		function contextual_help($help, $screen) {
			if ( 'settings_page_picasa-express-2' == $screen ) {
				$homepage = __('Plugin homepage','pe2');
				$messages = array(
					__('To receive access for private album press link under username. You will be redirected to Google for grant access. If you press "Grant access" button you will be returned to settings page, but access will be granted.','pe2'),
					__("In the album's images you have to press button with 'Image' button. The 'Gallery' will appear on the button and you can select several images. This can be happen if you use Thickbox, Lightbox or Highslide support.",'pe2'),
					__("By default images inserted in the displayed order. If you need control the order in gallery - enable 'Selection order'.", 'pe2'),
					__('To use external libraries like Thickbox, Lightbox or Highslide you need to install and integrate the library independently','pe2'),
					);
				$message = '<p>'.implode('</p><p>',$messages).'</p>';
				$help .= <<<HELP_TEXT
				<h5>Small help</h5>
				$message
				<div class="metabox-prefs">
					<a href="http://wott.info/picasa-express">$homepage</a>
				</div>
HELP_TEXT;
			}
			return $help;
		}

		/**
		 * Make the row from parameters for setting tables
		 */
		function make_settings_row($title, $content, $description='', $title_pars='', $description_pars='') {
			?>
					<tr valign="top" <?php echo $title_pars; ?>>
			        <th scope="row"><?php echo $title; ?></th>
			        <td>
						<?php echo $content; ?>
			        	<br />
			        	<span class="description" <?php echo $description_pars; ?>><?php echo $description; ?></span>
			        </td>
			        </tr>
			<?php
		}

		/**
		 * Show the main settings form
		 */
		function settings_form(){

			if ( 
				( isset($_GET['updated']) && 'true' == $_GET['updated'] ) ||
				( isset($_GET['settings-updated']) && 'true' == $_GET['settings-updated'] ) 
			) {
				// successfully performed an update, execute any custom
				// logic that can't be performed by automatic settings storage

				// change 'picasa_dialog' capability to new role
				$roles = get_editable_roles();

				foreach ( $roles as $role => $data) {
					$_role = get_role($role);
					if (isset($this->options['pe2_roles'][$role]) && $this->options['pe2_roles'][$role]) {
						$_role->add_cap('picasa_dialog');
					} else {
						$_role->remove_cap('picasa_dialog');
					}
				}

				// update the path to wordpress, using a PHP include so that
				// the file cannot be read by a request directly from the web
				// For more information, see line ~2160
				if(!file_exists(dirname(__FILE__).'/pe2-wp-path.php')){
					// create the file containing the path
					@file_put_contents(dirname(__FILE__).'/pe2-wp-path.php', '<? // AUTO-GENERATED by picasa-express-2 settings page, used in google token retrival
$pe2_wp_path = \''.str_replace('wp-admin', '', getcwd()).'\';');
				}// end if we need to generate the wordpress path storage file
			}// end successful settings update

			if ( isset($_GET['revoke']) ) {
				$response = $this->get_feed("https://www.google.com/accounts/AuthSubRevokeToken");
				if ( is_wp_error( $response ) ) {
					$message = __('Google return error: ','pe2').$response->get_error_message();
				} else {
					$message = __('Private access revoked','pe2');
				}
				delete_option('pe2_token');
				$this->options['pe2_token'] = '';
			}

			if ( isset($_GET['message']) && $_GET['message']) {
				$message = esc_html(stripcslashes($_GET['message']));
			}

			?>

			<div class="wrap">
			<div id="icon-options-general" class="icon32"><br /></div>
			<h2><?php _e('Picasa Express x2 settings', 'pe2')?></h2>

			<?php
				if (isset($message) && $message) {
					echo '<div id="picasa-express-x2-message" class="updated"><p><strong>'.$message.'</strong></p></div>';
				}
			?>

			<form method="post" action="options.php">
    			<?php settings_fields( 'picasa-express-2' ); ?>

				<?php if ($this->options['pe2_donate_link']) { ?>
				<a href="http://wott.info/donate/" target="_blank" style="position:relative;display:block;float:right;margin:10px;height: 10px;">
					<img src="http://lh6.ggpht.com/_P3-vavBhxe8/TKAJwocDI3I/AAAAAAAAAhQ/VZ9rmzWqXA4/s128/Paypal_button1.png" alt="PayPal donate" title="Donate to Wott via PayPal to support plugin development" width="128" height="62" />
				</a>
				<?php } ?>

				<input type="hidden" name="pe2_configured" value="1" />

				<h3><?php _e('Picasa access', 'pe2') ?></h3>
				<table class="form-table">

					<?php 
					$option = $this->options['pe2_roles'];
					$editable_roles = get_editable_roles();

					$pe2_roles = array();
					foreach( $editable_roles as $role => $details ) {
						$name = translate_user_role($details['name'] );
						$pe2_roles[] = "<label><input name=\"pe2_roles[$role]\" type=\"checkbox\" value=\"1\" ".checked(isset($option[$role]),true,false)."/> $name</label>";
					}
					$out = implode('<br/>', $pe2_roles);
					unset($pe2_roles);
					
					$this->make_settings_row(
						__('Assign capability to Roles', 'pe2'),
						$out,
						__('Roles for users who can use Picasa albums access via plugin', 'pe2')
					);

					$option = $this->options['pe2_level'];
					
					$this->make_settings_row(
						__('Picasa access level', 'pe2'),
						'<label><input type="radio" name="pe2_level" value="blog" '.checked($option,'blog',false).' onclick="jQuery(\'.picasa-site-user\').show();" /> '.__('Blog').'</label> &nbsp; '.
			        	'<label><input type="radio" name="pe2_level" value="user" '.checked($option,'user',false).' onclick="jQuery(\'.picasa-site-user\').hide();" /> '.__('User').'</label> ',
						__('Picasa user name ( including private album access ) defined for whole blog or for every user independently', 'pe2')
					);

					?>

				</table>

				<h3><?php _e('Display properties', 'pe2') ?></h3>
				<table class="form-table">

					<?php
					$user = $this->options['pe2_user_name'];

					if ('blog'==$this->options['pe2_level'] && $user) {
						$result = 'ok';
						$response = $this->get_feed("http://picasaweb.google.com/data/feed/base/user/$user?alt=rss&kind=album&hl=en_US");
						if ( is_wp_error( $response ) )
							$result = 'nok: '.$response->get_error_message();
						else if (!$this->get_item($response,'atom:id')) {
							$result = 'nok: wrong answer';
						}

						if (method_exists('WP_Http', '_getTransport')) {
							$ta = array(); $transports = WP_Http::_getTransport(array());
							foreach ($transports as $t) $ta[] = strtolower(str_replace('WP_Http_','',get_class($t)));
							if ($ta) $result = sprintf(__("checking user: %s transport: %s",'pe2'),$result,implode(',',$ta));
						} else if (method_exists('WP_Http', '_get_first_available_transport')) {
							$transport = WP_Http::_get_first_available_transport(array());
							if ($transport) {
								$transport_name = strtolower(str_replace('WP_HTTP_','',$transport));
								$result = sprintf(' '.__("checking user: %s - transport: %s",'pe2'),$result,$transport_name);
							}
							
						}
					} else $result='';

					// get our token variable
					if ($this->options['pe2_level'] == 'user') {
						global $current_user;
						$token = get_user_meta($current_user->data->ID,'pe2_token',true);
					} else {
						$token  = $this->options['pe2_token'];
					}

					$this->make_settings_row(
						__('Picasa user name for site', 'pe2'),
						'<input type="text" class="regular-text" name="pe2_user_name" value="'.esc_attr($user).'" />'.$result.
						(($token == null)?'<br /><a href="https://www.google.com/accounts/AuthSubRequest?next='.urlencode($this->google_authorize_plugin_URL.'?authorize').'&scope=http%3A%2F%2Fpicasaweb.google.com%2Fdata%2F&session=1&secure=0">'.__('Requesting access to private albums', 'pe2').'</a>':'<br/><a href="?page=picasa-express-2&revoke=true">'.__('Revoke access to private albums', 'pe2').'</a>'),
						(($token != null)?__('You have successfully authorized access to private albums.', 'pe2'):__('By clicking this link you will be redirected to the Google authorization page. Please use the same username as what is listed above to login before authorizing.', 'pe2')),
						'class="picasa-site-user" style="display:'.(('blog'==$this->options['pe2_level'])?'table-row':'none').'"'
					);

					$option = $this->options['pe2_save_state'];
					$this->make_settings_row(
						__('Save last state', 'pe2'),
						'<label><input type="checkbox" name="pe2_save_state" value="1" '.checked($option,'1',false).' /> '.__('Save last state in dialog', 'pe2').'</label> ',
						__('Save the last used username when it changes, the last selected album if you insert images, or the albums list if you insert an album shorcode', 'pe2'),
						'class="picasa-site-user" style="display:'.(('blog'==$this->options['pe2_level'])?'table-row':'none').'"'
					);

					$opts = array(
						1 => __('Picasa squire icon', 'pe2'),
						2 => __('Picasa squire grayscale icon', 'pe2'),
						3 => __('Picasa round icon', 'pe2'),
						4 => __('Picasa round grayscale icon', 'pe2'),
					);
					$option = $this->options['pe2_icon'];
					$out = '';
					foreach ($opts as $i=>$text) {
						$out .= '<label>';
						$out .= "<input type=\"radio\" name=\"pe2_icon\" value=\"$i\" ".checked($option,$i,false)." />";
						$out .= "<img src=\"{$this->plugin_URL}/icon_picasa$i.gif\" alt=\"$text\" title=\"$text\"/> &nbsp; ";
						$out .= '</label>';
			        	}

					$this->make_settings_row(
						__('Picasa icon', 'pe2'),
						$out,
						__('This icon marks the dialog activation link in the edit post page', 'pe2')
					);

					$opts = array(
						0 => __('None', 'pe2'),
						1 => __('Date', 'pe2'),
						2 => __('Title', 'pe2'),
						3 => __('File name', 'pe2'),
					);
					$option = $this->options['pe2_img_sort'];
					$out = '';
					foreach ($opts as $i=>$text) {
						$out .= '<label>';
						$out .= "<input type=\"radio\" name=\"pe2_img_sort\" value=\"$i\" ".checked($option,$i,false)." /> $text &nbsp; ";
						$out .= '</label>';
					}
					$this->make_settings_row(
						__('Sorting images in album', 'pe2'),
						$out,
						__('This option drives image sorting in the dialog', 'pe2')
					);

					$option = $this->options['pe2_img_asc'];
					$this->make_settings_row(
						__('Sorting order', 'pe2'),
						'<label><input type="radio" name="pe2_img_asc" value="1" '.checked($option,'1',false).' /> '.__('Ascending',  'pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_img_asc" value="0" '.checked($option,'0',false).' /> '.__('Descending', 'pe2').'</label> '
					);

					$option = $this->options['pe2_dialog_crop'];
					$this->make_settings_row(
						__('Selection dialog thumbnail style', 'pe2'),
						'<label><input type="radio" name="pe2_dialog_crop" value="1" '.checked($option,'1',false).' /> '.__('Crop into a square',  'pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_dialog_crop" value="0" '.checked($option,'0',false).' /> '.__('Scale proportionally', 'pe2').'</label> ',
						__('This applies to image thumbnails only, not album cover thumbnails')
					);

					$option = $this->options['pe2_max_albums_displayed'];
					$this->make_settings_row(
						__('Maximum albums displayed', 'pe2'),
						'<label>'.__('Max number of albums to display in the dialog:', 'pe2').'  <input type="text" name="pe2_max_albums_displayed" value="'.$option.'" size=4 />',
						__('Leave blank to display all albums',  'pe2').'</label> '
					);

					?>

				</table>

				<h3><?php _e('Image properties', 'pe2') ?></h3>

				<table class="form-table">

					<?php
					// ---------------------------------------------------------------------
					// single image thunbnail size (override)
					$option = $this->options['pe2_single_image_size'];
					preg_match('/(\w)(\d+)/',$option,$mode);
					if(strpos($option, '-c') !== false)
						$crop = true;
					else
						$crop = false;
					if (!$mode) $mode=array('','','');
					$this->make_settings_row(
						__('Single image thumbnail size', 'pe2'),
						'<input type="hidden" name="pe2_single_image_size" value="'.$option.'" />Scale: &nbsp; &nbsp;[ <label><input type="radio" name="pe2_single_image_size_mode" class="pe2_single_image_size" value="w" '.checked($mode[1], 'w', false).' /> '.__('width','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_image_size_mode" class="pe2_single_image_size" value="h" '.checked($mode[1], 'h', false).' /> '.__('height','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_image_size_mode" class="pe2_single_image_size" value="s" '.checked($mode[1], 's', false).' /> '.__('any','pe2').'</label> '.
						__(' ]&nbsp; &nbsp; proportionally to ','pe2').
						'<input type="text" name="pe2_single_image_size_dimension" class="pe2_single_image_size" style="width:60px;" id="pe2_single_image_size_dimension" value="'.$mode[2].'" />'.
						__(' pixels.','pe2').
						'<label> &nbsp; &nbsp; &nbsp; <input type="checkbox" name="pe2_single_image_size_crop" class="pe2_single_image_size" value="-c" '.checked($crop, true, false).' /> '.__(' Crop image into a square.','pe2').'</label> '
						,
						sprintf(__('Value \'%s\' will be used to set single image thumbnail size'),"<span id=\"pe2_single_image_size_message_option\">$option</span>"),
						'',
						'id="pe2_single_image_size_message" style="display:'.(($option) ? 'block' : 'none').';"'
					);
					?>
					<style type="text/css">
						input:disabled {
							background-color: #eee;
						}
					</style>
					<script type="text/javascript">
						function pe2_compute_image_size(mode,value,type) {
							var target_input = jQuery('input[name=pe2_' + type + '_size]');
							if(target_input.length == 0){
								// this is the large image size selection
								target_input = jQuery('input[name=pe2_' + type + '_limit]');
							}
							var val = target_input.val();
							
							// check for the case where it was just enabled after having
							// been disabled and saved
							if((val == '') || (val == 'w')){
								// override with some default
								val = 'w600';
							}

							// split into our parts
							var parts = {
								mode : val.substring(0, 1),
								size : val.replace(/^[a-z]*([0-9]*).*$/,'$1'),
								crop : val.replace(/^[a-z]*[0-9]*(.*)$/,'$1')};

							// override the particular part that was just changed
							parts[mode] = value;

							// store the value back in our target
							target_input.val(parts.mode+parts.size+parts.crop);

							// update the text that displays the setting being used
							jQuery('#pe2_' + type + '_size_message_option').text(parts.mode+parts.size+parts.crop);

							// if the target inputs type is hidden, then also trigger
							// the .change event (hiddens don't automatically trigger
							// the .change event for some reason)
							if(target_input.attr('type') == 'hidden'){
								target_input.trigger('change');
							}
						}// end function pe2_compute_image_size(..)
						// if mode changes, update image size
						jQuery('input[name=pe2_single_image_size_mode]').change(function(){ if (jQuery(this).attr('checked')) pe2_compute_image_size('mode',jQuery(this).val(), 'single_image'); });
						// if size changes, update image size
						jQuery('input[name=pe2_single_image_size_dimension]').change(function(){
							pe2_compute_image_size('size',jQuery('input[name=pe2_single_image_size_dimension]').val(), 'single_image');
						});
						// if crop changes, update image size
						jQuery('input[name=pe2_single_image_size_crop]').change(function(){
							pe2_determine_crop('single_image', this);
						});
						function pe2_determine_crop(name, obj){
							// use the checked selector to determine if the checkbox is 
							// checked or not
							if(jQuery('input[name=pe2_' + name + '_size_crop]:checked').length > 0){
								// the checkbox is checked
								pe2_compute_image_size('crop',jQuery(obj).val(), name);
							}else{
								// the checkbox is not checked
								pe2_compute_image_size('crop','',name);
							}
						}
					</script>
					<?php
					// ---------------------------------------------------------------------
					// single video thunbnail size (override)
					$option = $this->options['pe2_single_video_size'];
					preg_match('/(\w)(\d+)/',$option,$mode);
					if(strpos($option, '-c') !== false)
						$crop = true;
					else
						$crop = false;
					if (!$mode) $mode=array('','','');
					$this->make_settings_row(
						__('Single video thumbnail size', 'pe2'),
						'<input type="hidden" name="pe2_single_video_size" value="'.$option.'" />Scale: &nbsp; &nbsp;[ <label><input type="radio" name="pe2_single_video_size_mode" class="pe2_single_video_size" value="w" '.checked($mode[1], 'w', false).' /> '.__('width','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_video_size_mode" class="pe2_single_video_size" value="h" '.checked($mode[1], 'h', false).' /> '.__('height','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_single_video_size_mode" class="pe2_single_video_size" value="s" '.checked($mode[1], 's', false).' /> '.__('any','pe2').'</label> '.
						__(' ]&nbsp; &nbsp; proportionally to ','pe2').
						'<input type="text" name="pe2_single_video_size_dimension" class="pe2_single_video_size" style="width:60px;" id="pe2_single_video_size_dimension" value="'.$mode[2].'" />'.
						__(' pixels.','pe2').
						'<label> &nbsp; &nbsp; &nbsp; <input type="checkbox" name="pe2_single_video_size_crop" class="pe2_single_video_size" value="-c" '.checked($crop, true, false).' /> '.__(' Crop image into a square.','pe2').'</label> '
						,
						sprintf(__('Value \'%s\' will be used to set single video thumbnail size'),"<span id=\"pe2_single_video_size_message_option\">$option</span>"),
						'',
						'id="pe2_single_video_size_message" style="display:'.(($option) ? 'block' : 'none').';"'
					);
					?>
					<script type="text/javascript">
						// if mode changes, update image size
						jQuery('input[name=pe2_single_video_size_mode]').change(function(){ if (jQuery(this).attr('checked')) pe2_compute_image_size('mode',jQuery(this).val(), 'single_video'); });
						// if size changes, update image size
						jQuery('input[name=pe2_single_video_size_dimension]').change(function(){
							pe2_compute_image_size('size',jQuery('input[name=pe2_single_video_size_dimension]').val(), 'single_video');
						});
						// if crop changes, update image size
						jQuery('input[name=pe2_single_video_size_crop]').change(function(){
							pe2_determine_crop('single_video', this);
						});
					</script>
					<?

					// ---------------------------------------------------------------------
					// album thumbnail image size
					$this->make_settings_row(__('Album thumbnail size', 'pe2'),
						__('Album thumbnail images are inserted using the default thumbnail size.  This can be modified by using the <a href="options-media.php">Settings-&gt;Media</a> page.', 'pe2'));

					// ---------------------------------------------------------------------
					// large image size
					$option = $this->options['pe2_large_limit'];
					preg_match('/(\w)(\d+)/',$option,$mode);
					if (!$mode) $mode=array('','','');
					$this->make_settings_row(
						__('Large image size', 'pe2'),
						'<label><input type="checkbox" name="pe2_large_limit" value="'.$option.'" '.checked(($option)?1:0,1,false).' /> '.__('Set / Limit: ','pe2').'</label> '.
						'<label> &nbsp; &nbsp;[ <input type="radio" name="pe2_large_size_mode" class="pe2_large_limit" value="w" '.checked($mode[1], 'w', false).' '.disabled(($option)?1:0,0,false).' /> '.__('width','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_large_size_mode" class="pe2_large_limit" value="h" '.checked($mode[1], 'h', false).' '.disabled(($option)?1:0,0,false).' /> '.__('height','pe2').'</label> &nbsp; '.
						'<label><input type="radio" name="pe2_large_size_mode" class="pe2_large_limit" value="s" '.checked($mode[1], 's', false).' '.disabled(($option)?1:0,0,false).' /> '.__('any','pe2').' ]&nbsp; &nbsp; </label> '.
						__(' proportionally to ','pe2').
						'<input type="text" name="pe2_large_size_dimension" class="pe2_large_limit" style="width:60px;" id="pe2_large_size" value="'.$mode[2].'" '.disabled(($option)?1:0,0,false).' />'.
						__(' pixels.','pe2').
						'<label> &nbsp; &nbsp; &nbsp; <input type="checkbox" name="pe2_large_size_crop" class="pe2_large_limit" value="-c" '.checked($crop, true, false).' /> '.__(' Crop image into a square.','pe2').'</label> '
						,
						sprintf(__('Value \'%s\' will be used to set / limit large image'),"<span id=\"pe2_large_size_message_option\">$option</span>"),
						'',
						'id="large-limit-message" style="display:'.(($option) ? 'block' : 'none').';"'
					);
					?>
					<script type="text/javascript">
						jQuery('input[name=pe2_large_limit]').change(function(){
							if (jQuery(this).attr('checked')) {
								// the checkbox is set
								jQuery('input.pe2_large_limit').removeAttr('disabled');
								jQuery('#large-limit-message').show();

								// set the default for the input boxes
								jQuery('input[name=pe2_large_size_mode][value=w]').attr('checked', 'true');
								jQuery('input[name=pe2_large_size_dimension]').val('600');

								// call the calculation function for each section
								pe2_compute_image_size('mode',jQuery('input[name=pe2_large_size_mode]').val(), 'large');
								pe2_compute_image_size('size',jQuery('input[name=pe2_large_size_dimension]').val(), 'large');
							} else {
								jQuery('input.pe2_large_limit').removeAttr('checked').attr('disabled','disabled');
								jQuery('input[name=pe2_large_size]').val('');
								jQuery('#pe2_large_size_message_option').text('');
								jQuery('input[name=pe2_large_limit]').val('');
								jQuery('#large-limit-message').hide();
							}
						});
						// if mode changes, update image size
						jQuery('input[name=pe2_large_size_mode]').change(function(){ if (jQuery(this).attr('checked')) pe2_compute_image_size('mode',jQuery(this).val(), 'large'); });
						// if size changes, update image size
						jQuery('input[name=pe2_large_size_dimension]').change(function(){
							pe2_compute_image_size('size',jQuery('input[name=pe2_large_size_dimension]').val(), 'large');
						});
						// if crop changes, update image size
						jQuery('input[name=pe2_large_size_crop]').change(function(){
							pe2_determine_crop('large', this);
						});
					</script>
					<?php

					$option = $this->options['pe2_caption'];
					$this->make_settings_row(
						__('Display caption', 'pe2'),
						'<label><input type="checkbox" name="pe2_caption" value="1" '.checked($option,'1',false).' /> '.__('Show the caption under thumbnail image', 'pe2').'</label> '
					);

					$option = $this->options['pe2_video_overlay'];
					$this->make_settings_row(
						__('Use video overlay', 'pe2'),
						'<label><input type="checkbox" name="pe2_video_overlay" value="1" '.checked($option,'1',false).' /> '.__('Use the JavaScript video overlay on thumbnails for videos on the main display page/post and in the selection dialog', 'pe2').'</label> '
					);

					$option = $this->options['pe2_title'];
					$this->make_settings_row(
						__('Add caption as title', 'pe2'),
						'<label><input type="checkbox" name="pe2_title" value="1" '.checked($option,'1',false).' /> '.__('Show the caption by mouse hover tip', 'pe2').'</label> '
					);

					$opts = array (
						'none'     => __('No link', 'pe2'),
						'direct'   => __('Direct link', 'pe2'),
						'picasa'   => __('Link to Picasa Web Album', 'pe2'),
						'lightbox' => __('Lightbox', 'pe2'),
						'thickbox' => __('Thickbox (External)', 'pe2'),
						'thickbox_integrated' => __('Thickbox (Integrated Wordpress version)', 'pe2'),
						'thickbox_custom' => __('Thickbox (Custom from this plugin)', 'pe2'),
						'highslide'=> __('Highslide', 'pe2'),
					);
					$is_gallery = array (
						'none'     => 'false',
						'direct'   => 'false',
						'picasa'   => 'false',
						'lightbox' => 'true',
						'thickbox' => 'true',
						'thickbox_integrated' => 'true',
						'thickbox_custom' => 'true',
						'highslide'=> 'true',
					);
					$is_gallery_js = 'var is_gallery = { ';
					foreach ($is_gallery as $key=>$val) {
						$is_gallery_js .= "$key:$val,";
					}
					$is_gallery_js = trim($is_gallery_js, ',').' };';
					?>
					<script type="text/javascript">
					function handle_gallery_properties(t) {
						<?php echo $is_gallery_js; ?>

						if (is_gallery[t]) {
							jQuery('#gallery_properties').show();
							jQuery('#gallery-message').show();
							jQuery('#nogallery_properties').hide();
						} else {
							jQuery('#gallery_properties').hide();
							jQuery('#gallery-message').hide();
							jQuery('#nogallery_properties').show();
						}
					}
					</script>
					<?php

					$out = '<select name="pe2_link" onchange="handle_gallery_properties(this.value);">';
					$option = $this->options['pe2_link'];
					foreach ($opts as $key => $val ) {
						$out .= "<option value=\"$key\" ".selected($option, $key, false ).">$val</option>";
					}
					$out .= '</select>';
					$this->make_settings_row(
						__('Link to larger image', 'pe2'),
						$out,
						__('To use external libraries like Thickbox, Lightbox or Highslide you need to install and integrate the library independently','pe2'),
						'',
						'id="gallery-message" style="display:'.(($is_gallery[$option]=='true') ? 'block' : 'none').';"'
					);

					$option = $this->options['pe2_relate_images'];
					$this->make_settings_row(
						__('Relate all of a post\'s images', 'pe2'),
						'<label><input type="checkbox" name="pe2_relate_images" value="1" '.checked($option,'1',false).' /> '.__('If using Thickbox, Lightbox or Highslide, relate all images in the page/post together for fluid next/prev navigation', 'pe2').'</label> '
					);

					$opts = array (
						'none'   => __('None'),
						'left'   => __('Left'),
						'center' => __('Center'),
						'right'  => __('Right'),
					);
					$option = $this->options['pe2_img_align'];
					$out = '';
					foreach ($opts as $key => $val ) {
						$out .= "<input type=\"radio\" name=\"pe2_img_align\" id=\"img-align$key\" value=\"$key\" ".checked($option, $key, false)." /> ";
						$out .= "<label for=\"img-align$key\" style=\"padding-left:22px;margin-right:13px;\" class=\"image-align-$key-label\">$val</label>";
					}
					$this->make_settings_row(
						__('Image alignment', 'pe2'),
						$out
					);

					$option = $this->options['pe2_auto_clear'];
					$this->make_settings_row(
						__('Auto clear: both', 'pe2'),
						'<label><input type="checkbox" name="pe2_auto_clear" value="1" '.checked($option,'1',false).' /> '.__('Automatically add &lt;p class="clear"&gt;&lt;/p&gt; after groups of images inserted together', 'pe2').'</label> '
					);

					$this->make_settings_row(
						__('CSS Class', 'pe2'),
						'<input type="text" name="pe2_img_css" class="regular-text" value="'.esc_attr($this->options['pe2_img_css']).'"/>',
						__("You can define default class for images from theme's style.css", 'pe2')
					);
					$this->make_settings_row(
						__('Style', 'pe2'),
						'<input type="text" name="pe2_img_style" class="regular-text" value="'.esc_attr($this->options['pe2_img_style']).'"/>',
						__('You can hardcode some css attributes', 'pe2')
					);
					?>

				</table>

				<h3><?php _e('Gallery properties', 'pe2') ?></h3>
				<p>
				<?php _e('Multiple images from an album can be grouped into a gallery by Lightbox, Thickbox or Highslide and viewed sequentially with embedded navigation.', 'pe2') ?>
				</p>

			<div id="nogallery_properties" style="<?php echo ($is_gallery[$this->options['pe2_link']]=='true') ? 'display:none;' : 'display:block;'?>">
				<p><?php _e('To view and change properties you have to select Thickbox, Lightbox or Highslide support for the images above', 'pe2') ?></p>
			</div>
			<div id="gallery_properties" style="<?php echo ($is_gallery[$this->options['pe2_link']]=='false') ? 'display:none;' : 'display:block;'?>">

				<table class="form-table">

					<?php
					// display tag options
					$this->make_settings_row(
						__('Photo tag options', 'pe2'),
						'<input name="pe2_featured_tag" type="checkbox" id="pe2_featured_tag" value="1" '.checked('1', get_option('pe2_featured_tag'),false).'/> '.
						'<label for="pe2_featured_tag">'.__('Include photos from albums only if they contain the "Featured" tag').'</label><br />'.
						'<label for="pe2_additional_tags" style="vertical-align: top;">'.__('Additional tag(s) required').'</label> '.
						'<div style="display: inline-block;"><input type="text" name="pe2_additional_tags" id="pe2_additional_tags" class="regular-text" value="'.esc_attr($this->options['pe2_additional_tags']).'"/><br/><span style="font-size: 9px;">('.__('Separate multiple tags by commas.  NOTE: currently Google requires private album access for tags to work').')</span></div>'
					);

					// remaining gallery options
					$this->make_settings_row(
						__('Selection order', 'pe2'),
						'<label><input type="checkbox" name="pe2_gal_order" value="1" '.checked($this->options['pe2_gal_order'],'1',false).' /> '.__("Click images in your preferred order", 'pe2').'</label>'
					);

					$option = $this->options['pe2_gal_align'];
					$out = '';
					foreach ($opts as $key => $val ) {
						$out .= "<input type=\"radio\" name=\"pe2_gal_align\" id=\"gal-align$key\" value=\"$key\" ".checked($option, $key, false)." /> ";
						$out .= "<label for=\"gal-align$key\" style=\"padding-left:22px;margin-right:13px;\" class=\"image-align-$key-label\">$val</label>";
					}
					$this->make_settings_row(
						__('Gallery alignment', 'pe2'),
						$out
					);

					$this->make_settings_row(
						__('CSS Class', 'pe2'),
						'<input type="text" name="pe2_gal_css" class="regular-text" value="'.esc_attr($this->options['pe2_gal_css']).'"/>',
						__("You can define default class for images from theme's style.css", 'pe2')
					);
					$this->make_settings_row(
						__('Style', 'pe2'),
						'<input type="text" name="pe2_gal_style" class="regular-text" value="'.esc_attr($this->options['pe2_gal_style']).'"/>',
						__('You can hardcode some css attributes', 'pe2')
					);
					?>

				</table>
			</div>

				<h3><?php _e('Advertising', 'pe2') ?></h3>

				<table class="form-table">

					<?php
					$this->make_settings_row(
						__('Footer link', 'pe2'),
						'<label><input type="checkbox" name="pe2_footer_link" value="1" '.checked($this->options['pe2_footer_link'],'1',false).' /> '.__('Enable footer link "With Picasa plugin by Wott"','pe2').'</label>'
					);

					$this->make_settings_row(
						__('PayPal donation banner', 'pe2'),
						'<label><input type="checkbox" name="pe2_donate_link" value="1" '.checked($this->options['pe2_donate_link'],'1',false).' /> '.__('Enable PayPal banner on this page','pe2').'</label>'
					);


					?>

				</table>

				<p class="submit">
			    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
			    </p>

			</form>
			</div>
			<?php
		}
	}
}

if (isset($_GET['authorize'])&&!defined('ABSPATH')) {

	if (!isset($_GET['token'])||!$_GET['token']||strlen($_GET['token'])>256) {
		header('Location: '.preg_replace('/wp-content.*/','',$_SERVER["REQUEST_URI"]).'wp-admin/options-general.php?page=picasa-express-2');
		die();
	}

	// require wp-load so that wordpress loads allowing us to perform the updates
	// to the appropriate setting
	if(file_exists(dirname(__FILE__).'/pe2-wp-path.php')){
		// include the pe2-wp-path.php to define the wordpress root
		// (this allows a customized path (sometimes necessary in certain
		// wordpress installations) to be set when saving the pe2 preferences page 
		// and written to the file system so it can be loaded here prior 
		// to any wordpress filters or functionality being available)
		// (using an include that sets a variable so that if the file is
		// accessed from the web nothing is visible, thus not causing a
		// security problem)
		include(dirname(__FILE__).'/pe2-wp-path.php');
	}
	if(!isset($pe2_wp_path)){
		// for some reason the include doesn't exist (preferences haven't
		// been written, or web server doesn't have write access to the
		// plugin directory) or the include didn't set the appropriate
		// variable.
		// We have no choice but to determine the path as best as we can 
		// and hope it works with this installation
		$pe2_wp_path = preg_replace('/wp-content.*/','',__FILE__);
	}
	// require wp-load.php for the core wordpress functions we need
	require_once($pe2_wp_path.'wp-load.php');

	// create our instance and continue updating
	if (!isset($pe2_instance)) $pe2_instance = new PicasaExpressX2();

	if ('user' == $pe2_instance ->options['pe2_level'] && isset($_GET['user']) ) {
		$user_id = sanitize_text_field($_GET['user']);
		$user = new WP_User( $user_id );

		global $wp_roles;
		if ( ! isset( $wp_roles ) )	$wp_roles = new WP_Roles();

		$allow = false;
		foreach ( $user->roles as $role) {
			if (isset($wp_roles->roles[$role]['capabilities']['picasa_dialog'])) {
				$allow=true; break;
			}
		}

		if (!$allow) {
			header('Location: '.preg_replace('/wp-content.*/','',$_SERVER["REQUEST_URI"]).'wp-admin/profile.php');
			die();
		}
	}

	$response = $pe2_instance->get_feed("https://www.google.com/accounts/AuthSubSessionToken",sanitize_text_field($_GET['token']));

	$message='';
	if (is_wp_error($response)) {
		$message = 'Can\'t request token: ' .$response->get_error_message();
	} else if ($response) {
		$lines  = explode("\n", $response);

		// grab our current logged in user
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;

		// go thorugh all lines in the reponse looking for the token
		foreach ($lines as $line) {
			$pair = explode("=", $line, 2);
			if (0==strcasecmp($pair[0],'token')) {
				if ((get_option('pe2_level') == 'user') && isset($user_id))
					update_user_meta($user_id,'pe2_token',sanitize_text_field($pair[1]));
				else
					update_option('pe2_token',sanitize_text_field($pair[1]));
				$message = 'Private access received';
			}
		}
	}

	if (isset($user_id))
		header('Location: '.preg_replace('/wp-content.*/','',$_SERVER["REQUEST_URI"]).'wp-admin/profile.php?message='.rawurlencode($message));
	else
		header('Location: '.preg_replace('/wp-content.*/','',$_SERVER["REQUEST_URI"]).'wp-admin/options-general.php?page=picasa-express-2&message='.rawurlencode($message));
	die();

} else {
	if (!isset($pe2_instance)) $pe2_instance = new PicasaExpressX2();
}


// add the pe2 display css file
add_action('init','pe2_add_display_css');
function pe2_add_display_css(){
	// add the pe2-display.css file
	wp_enqueue_style('pe2-display.css',plugins_url('/pe2-display.css', __FILE__), null, PE2_VERSION);
}// end function pe2_add_display_css()


// to use the default thickbox script with wordpress:
if(get_option('pe2_link') == 'thickbox_integrated'){
	// they chose the option to use the internal Wordpress version of Thickbox
	add_action('init','pe2_add_thickbox_script');
}
function pe2_add_thickbox_script(){
	// add in the thickbox script built into wordpress if not in the admin
	// and the user has selected thickbox as the display method
	if(!is_admin()){
		// not in the admin, so we can load thickbox
		wp_enqueue_script('jquery');
		wp_enqueue_script('thickbox',null,array('jquery'));
		wp_enqueue_style('thickbox.css', '/'.WPINC.'/js/thickbox/thickbox.css', null, '1.0');
	}
}// end function pe2_add_thickbox_script()


// to use a custom thickbox script for display:
if(get_option('pe2_link') == 'thickbox_custom'){
	// they chose the option to use the custom thickbox from this plugin
	add_action('init', 'pe2_add_custom_thickbox_script');
}
function pe2_add_custom_thickbox_script(){
	// add in the thickbox script built into wordpress if not in the admin
	// and the user has selected thickbox as the display method
	if(!is_admin()){
		// not in the admin, so we can load thickbox
		wp_enqueue_script('jquery');
		wp_enqueue_style('thickbox.css', '/'.WPINC.'/js/thickbox/thickbox.css', null, PE2_VERSION);
		add_action('wp_footer', 'pe2_add_custom_thickbox_config');
	}
}// end function pe2_add_custom_thickbox_script()
function pe2_add_custom_thickbox_config(){
	?><script type='text/javascript'>
/* <![CDATA[ */
var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"<?= str_replace('/', '\/', includes_url('/js/thickbox/loadingAnimation.gif')) ?>","closeImage":"<?= str_replace('/', '\/', includes_url('/js/thickbox/tb-close.png')) ?>"};
/* ]]> */
</script>
<script src="<?= plugins_url('/thickbox-custom.js', __FILE__) ?>?ver=<?= PE2_VERSION ?>"></script>
<?
}// end function pe2_add_custom_thickbox_config()


// function to add the play button image overlay over video images
if(get_option('pe2_video_overlay') == '1'){
	// they chose the option to use the video overlay, enable it
	add_action('wp_footer','pe2_add_video_overlay');
	add_action('pe2_get_images_footer','pe2_add_video_overlay_filter');
}
function pe2_add_video_overlay($dialog = false){
	?><script>
jQuery(document).ready(function(){
	// ready event, find any images with class="video" and append
	// a div after them to style, thus allowing us to style it
	// to create the "video play button" overlay
	jQuery('img[type=video]').each(function(){
		// for each video image on the page, determine it's height/
		// width and then set the video_overlay to the matching
		// dimensions
		pe2_add_video_overlay_helper(jQuery(this));
	});
});
function pe2_add_video_overlay_helper(image_obj){
	// check to make sure we have our image dimensions
	if((image_obj.width() > 0) && (image_obj.height() > 0)){
		// we're good, we have image dimensions.  wait one more second
		// to make sure they're correct, then insert our overlay
		setTimeout(function(){pe2_add_video_overlay_helper_run(image_obj)}, 1000);
	}else{
		// no dimensions yet, delay and retry
		setTimeout(function(){pe2_add_video_overlay_helper(image_obj)}, 1000);
	}
}// pe2_add_video_overlay_helper(..)
function pe2_add_video_overlay_helper_run(image_obj){
	// add the HTML for the overlay
	image_obj.wrap('<div class="play_overlay_wrapper"></div>');
	image_obj.after('<span class="play_overlay" style="width: ' + image_obj.width() + 'px; height: ' + image_obj.height() + 'px; <?
		
	// determine which margin we need to adjust, based on where we're
	// being called
	if($dialog){
		// we're being called from the image selection dialog
		?>margin-left: 5px; margin-top: -' + (image_obj.height() + 5) + 'px;<?
		
	}else{
		// we're being called from wordpress display
		?>margin-left: -' + (image_obj.width() + 4) + 'px; margin-top: 2px;<?
	}
	
	// finish up the JS
	?>"></span>');
}// end function pe2_add_video_overlay_helper_run(..)
</script>
<?
}// end function pe2_add_video_overlay(..)
function pe2_add_video_overlay_filter($output){
	// get the output of the pe2_add_video_overlay() function and append it
	// to the $output
	ob_start();
	pe2_add_video_overlay(true);
	$output .= ob_get_clean();

	// return the modified output
	return $output;
}// end function pe2_add_video_overlay_filter(..)
