=== Plugin Name ===
Contributors: wott, gjanes
Donate link: http://wott.info/donate/
Tags: google+, picasaweb, picasa, photo, gallery, image, album, highslide, lightbox, private, wpmu, user, sidewide
Requires at least: 2.8
Tested up to: 3.5.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Browse, search and select photos/albums from any Google+/Picasa Web album and add them to your posts/pages.

== Description ==

Browse your Google+/Picasa Web albums and select images or whole albums to insert into your posts/pages.

*	Use your Google user to get albums ( the username can be stored in settings )
*	**Private** album access after granting access via Google service
*	Select albums / images from GUI listing by album cover and name
*	Display only photos from selected album with "Featured" (or a custom) tag
*   Gallery and image shortcodes for display of entire album or selected images
*	**Wordpress MU** support - sidewide activation, users, roles

Additional settings: 

*	Image link: none, direct, Google+/Picasa image page, **custom thickbox with plugin**, **wordpress thickbox**, **3rd-party thickbox**, **3rd-party lightbox** and **3rd-party highslide** with gallery
*	Caption under image or/and in image/link title
*	Alignment for images and gallery using standard CSS classes
*	Define **Roles** who are allowed to use the plugin
*	Switch from blog to **user level** for storing the user and private access token
*	Settings for single-image thumbnail size, single-video thumbnail size and large image size limit

And by design:

*	Support native Wordpress image and link dialog for album image thumbnail size
*	Multilanguage support

== Installation ==

1. Upload `picasa-express` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use Settings link under 'Settings' -> 'Picasa Express x2' and set user name and other parameters. If you need **private albums** access (**required for tag searches**) use the link under the username field to request access from Google.
4. In the edit post/page screen use the picasa icon next to 'Add Media' for image/album selection dialog
5. Select an album or image to insert into your post/page:
	* To insert a whole album, click the "Album" button so it toggles to "Shortcode", then click on the album you wish to insert
	* To insert an image, click on the album the image is in, then select one or more images.  When done selecting images, click "Insert" to insert all of the selected images

== Frequently Asked Questions ==

= After upgrade, selection dialog displays blank screen =
If the upgrade to version 2 causes the selection dialog to no longer return
albums and photos, this is probably caused due to changes in the token
authorization process.

Changes were made to the token authorization process to fix issues on Google's
side, and these changes have altered the URL used to generate the token.  This
can cause Google to think you're providing an invalid token.

To fix the issue, simply revoke access (through the plugin) and request it again.

= How can I select several images to insert at a time? =

When browsing the album's images you may click on multiple photos to insert at once.  These images will be inserted into the post as using the thumbnail size you have defined in "Single image thumbnail size" in settings.

If you have selected the option to "Relate all of a post's images" and you are
using a link type of one of the thickbox, lightbox or highslide libraries, all
of the photos within the post will be related for next/prev navigation within
the display window.

= How can I select several images for "Gallery" display? =
There are two options to filter images in a "Gallery" display:

1. Use tags to filter photos in an album so that only photos with a specific
tag are displayed.

This is done by inserting an "Album" shortcode, but in the "Options" prior to inserting populate the "Photo tag options" to select photos with the "Featured" tag, or a customized tag name typed into the text box.

NOTE - filtering by tags requires private album access.  This is a
Google+/picasaweb requirement.

2. Manually select the photos (and optionally their sort order).

This is done while browsing an album's photos, click the "Image" button.
After clicking the "Image" button, it will switch to "Gallery" indicating that
any photos selected this time will be inserted into the post in "Gallery"
format using the default blog thumbnail size.

= I want private albums browsing =

On Setting page press link under the username field titled "Request proviate albums access". You will be redirected to Google and asked to grant access. If you press "Grant access" button you will be returned to settings page, but access will be granted.

After successfully authorizing the access for the blog, the next time you open
the album / image selection dialog, you will see all of your photos and
albums, including any private ones.

= I revoked access to my server on Google's site, and now the plugin doesn't work =

On revoking access through Google instead of this plugin, Google will not inform your server. If you revoke access on Google's side, you have to clean the option 'pe2_token' in the DB or click the "Revoke access to private albums" link on the settings page.

= I want another feature =

Go to my page [Garbage Collector 2.0](http://wott.info/picasa-express/ "Picasa Express x2 home page") and write a comment describing the feature you are looking for. I will review it under next release planning. Of course if you donate, the request might receive extra attention, but I build the plugin for my wife and THEN for the community.

If nobody responds, you might consider sending an email to either developer.

= I open selection dialog but user always 'invalid' =

Open Google+/picasaweb and select an album. In the URL you can find your username like 'http://picasaweb.google.com/username/...'.  In addition, the username appears to always work with your google account's email address.

If you are sure you have the correct Picasa username, try your google email address.  If this still doesn't work, check your hosting provider's PHP preferences - some hosting companies disable connect functions and Wordpress can not request data from other sites. 

If you revoke access to private albums via Google site and not this plugin, see message above.
 
= Plugin has a bug. How can I fix it? =

Open a support thread or contact me on [Garbage Collector 2.0](http://wott.info/picasa-express/ "Picasa Express x2 home page")

== Screenshots ==

1. Opening the album/image selection dialog
2. Album selection dialog - either open an album to view/select individual
photos. Or switch to shortcode mode and insert a whole album shortcode for a
gallery of photos from an entire album.
3. Photo selection dialog - select one or more photos to insert into the post.
Or, switch to gallery and select images to insert together into a
small-thumbnail gallery.
4. Plugin settings


== Changelog ==

= todo =
* Selection enhancement - use mouse click mods: Shift (for lines), Ctrl (for columns) and Ctrl-A for all images
* Recently uploaded images
* Add the button to Visual toolbar of Tiny editor in full screen mode 
* Embedded video support (currently doesn't appear to be allowed by Google)

= 2.0.2 =
A bugfix release to correct bugs with the options page and requesting auth
tokens from an SSL-based site.

= 2.0.1 =
A small bugfix release to correct a fatal error when gathering the transport
for user-based access levels.

= 2.0 =
A major change release that adds a lot of new functionality:
* Fixed a bug introduced by Google+ that was preventing private albums access
* Added video support - video thumbnail images have a "video play button overlay", and the link opens in a new tab for viewing on Google+
* Added filtering albums with tags
* Added built-in customized thickbox option
* Added single-image shortcode
* Single-image insert method now allows selecting multiple images to insert together

= 1.5.4 =
Picasa change URL for origin image so /s0

= 1.5.3 =
* Wordpress 3.3 fixes
* Gallery shortcode: 
 * `class`, `style` and `align` for gallery
 * `img_class`,`img_style` and `img_align` for images inside gallery
    ( align can be `none`, `left`, `right` or `center` )
 * `img_sort` for sort images ( possible values: `none`, `date`, `title`, `file` or `random` )
 * `img_asc=1` for ascending sort otherwise will be descending sort
 * `caption=1` for use image description as image caption
 * `use_title=1` for use description as image title
 * `tag` for enveloping tag ( `div` by default )
 * `thumb_w` and `thumb_h` for thumnails size and `thumb_crop=1` if you need to crop thumbnail to square 
 * `large_size` for limit opened image
 * `link` for enveloping link (possible values: `none`, `direct`, `picasa`, `thickbox`, `lightbox` or `highslide` )
 * `limit` to limit number of images displayed
 * `hide_rest=1` include images over limit but with display:none. It's for gallery script which show all images including hidden.

= 1.5.2 =
Wordpress 3.2 fixes

= 1.5 =
* Save last state
* Can limit the big size of images
* Revoke access to private albums from settings

* avoid some warnings for PHP4
* Add error handling in several cases
* remove SSL verification ( some hosts report the problem with ssl verification - thanks to streetdaddy)
* increase timeout for connection with Google Picasa
* add Picasa username test in settings
* Envelop html special chars in titles

= 1.4 =
* Some code was re-factored to be more extensible
* Warning for non standard image library. Help and links. 
* Donate banner and "power by" link in the footer (of course you can disable link and banner in the settings)
* New smart size for thumbnails
* Options on fly
* Insert Picasa album by shortcode

= 1.3 =
* Define roles which capable to use the plugin
* Switch from blog to user level for store Picasa user and private access token 
* Wordpress MU support - sidewide activation, users, roles

= 1.2 =
* Finally make compatible with old PHP versions
* Access to private albums via granting on Google page. See FAQ for more details 
* Reload button in dialog to retreive last changes

= 1.1 =
* Remove STATIC - should work with old PHP version. Optimize hooks and setting. Keep settings after deactivation.
* Change Picasa request method. Remove WP cache - changes displayed immediatelly.
* Add sorting ( date, title and file name , asc and desc ) for images in the dialog in settings. Without sorting should be displayed as in PicasaWeb.
* Add ordering for images in gallery ( by clicking )
* Add **Highslide** support

= 1.0 =
* First public release

== Upgrade Notice ==

= 2.0.2 =
It's a bugfix update, do it!

= 2.0.1 =
A small bugfix with user-based access level installations

= 2.0 =
A major change release adding functionality and fixing bugs introduced by Google+.  Some highlights:
* Added video support - video thumbnail links to Google+ for viewing
* Added filtering albums with tags
* Single-image insert method now allows selecting multiple images to insert together

= 1.5.4 =
Very small tweak to change the URL for origin image to /s0 for the large size.

= 1.5.3 =
Some fixes for WP 3.3
Add more parameters to gallery shortcode

= 1.5.2 =
Some fixes for WP 3.2
Remove depricated function and so on

= 1.5 =
Originally I plan to release Save Last State feature.
But I receive a lot of questions and have to add several small changes to prevent most issued problems.

= 1.4 =
Some code is refactored to be more correct and extensible. Please let me know if you find something wrong. Also I change the URLs for thumnails, but can't find documentations for this changes. Let me know if your thumbnails will not shows.

A lot of enhacement is in. I hope so you will work easely and fast with images on your blog. The full feature list and description is in [plugin version page](http://wott.info/picasa-express/new-version-1-4-of-picasa-express-plugin-for-wordpress/ "Picasa Express x2 v1.4")

Developers can add button for use plugin in form like custom_posts. For details please visit [my site](http://wott.info/picasa-express/new-version-1-4-of-picasa-express-plugin-for-wordpress/ "Picasa Express x2 v1.4")

= 1.3 = 
By this version new capability is added to manage plugin access. By updating, take a look into setting and check roles who need the plugin access and **save settings** in any case.

You can additionally use user level access to Picasa albums - when Picasa username ( and private album access token ) defined for every user. Users who have administartive privelegies automatically get blog Picasa username and token on **first profile access**.

Works with Wordpress MU - can be activated sidewide or per blog. In both cases every blog has own data as described above.

= 1.2 = 
Access to private albums via AuthSub.
Reload button in dialog

= 1.1 =
New options is in settings, but defaults make behaviour as before.
